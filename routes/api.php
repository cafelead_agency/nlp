<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {
    /**
     * User Action
     */

    Route::post('/user_info', 'UsersController@user_info')->middleware('check_token');
    Route::post('/add_new_user', 'UsersController@add_new_user');
    Route::post('/user_login', 'UsersController@user_login');
    Route::post('/user_do_login', 'UsersController@user_do_login');
    Route::post('/sms', 'UsersController@sms');
    Route::post('/update_user_info', 'UsersController@update_user_info')->middleware('check_token');

    /**
     * Users Cart Action
     */
    Route::post('/user_cart_info', 'UsersCartController@user_cart_info');
    Route::post('/add_new_user_cart', 'UsersCartController@add_new_user_cart');
    Route::post('/remove_user_cart', 'UsersCartController@remove_user_cart');
    Route::post('/buy_user_cart', 'UsersCartController@buy_user_cart');


});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


