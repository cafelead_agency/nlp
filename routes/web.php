<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Admin panel Route
 */

Route::group(['prefix' => 'mgmt_portal' , 'namespace' => 'AdminController'] , function (){


    Route::get('/','HomeController@index')->name('admin_dashboard');
    /**
     * login & logout
     */
    Route::get('/admin_users_login','HomeController@login')->name('admin_users_login');
    Route::post('/admin_users_login_post','HomeController@dologin')->name('admin_users_login_post');
    Route::get('/logout','HomeController@logout')->name('admin_logout');


    /**
     * Admin Users Route
     */

    Route::get('/admin_users_index','AdminUsersController@index')->name('admin_users_index');
    Route::get('/admin_users_create','AdminUsersController@create')->name('admin_users_create');
    Route::get('/admin_users_edit/{id}','AdminUsersController@edit')->name('admin_users_edit');
    Route::post('/admin_users_store','AdminUsersController@store')->name('admin_users_store');
    Route::post('/admin_users_update/{id}','AdminUsersController@update')->name('admin_users_update');
    Route::get('/admin_user_permission','AdminUsersController@admin_user_permission')->name('admin_user_permission');
    Route::get('/admin_user_upload_img','AdminUsersController@admin_user_upload_img')->name('admin_user_upload_img');
    Route::get('/admin_user_is_active','AdminUsersController@admin_user_is_active')->name('admin_user_is_active');
    Route::get('/admin_user_is_delete','AdminUsersController@admin_user_is_delete')->name('admin_user_is_delete');
    Route::get('/admin_user_change_password','AdminUsersController@admin_user_change_password')->name('admin_user_change_password');



    /**
     * Admin Users Permission
     */
    Route::get('/permission/{id}' , 'RoleController@index')->name('permission_list');
    Route::get('/permission/create/{id}' , 'RoleController@create')->name('permission_create_role');
    Route::post('/permission/store/{id}' , 'RoleController@store')->name('permission_store_role');

    Route::get('/create/permissions/{id}' , 'PermissionController@create')->name('user_create_permission');
    Route::post('/store/permissions/{id}' , 'PermissionController@store')->name('user_store_permission');
    Route::delete('/delete/permissions/{user_id}/{per_id}' , 'RoleController@destroy')->name('user_delete_permission');


    Route::get('/create/subpermissions/{id}' , 'SubPermissionController@create')->name('user_create_subpermission');
    Route::post('/store/subpermissions/{id}' , 'SubPermissionController@store')->name('user_store_subpermission');
    Route::get('/select/sub' , 'SubPermissionController@select_sub')->name('user_select_sub_subpermission');



    /**
     * Admin Panel Course Dore Category
     */

    Route::get('/Course_dore_cat' , 'CourseDoreCatController@course_dore_cat_index')->name('Course_dore_cat_index');
    Route::get('/Course_dore_cat/create' , 'CourseDoreCatController@course_dore_cat_create')->name('Course_dore_cat_create');
    Route::get('/Course_dore_cat/edit/{id}' , 'CourseDoreCatController@course_dore_cat_edit')->name('Course_dore_cat_edit');
    Route::get('/Course_dore_cat_upload_img','CourseDoreCatController@Course_dore_cat_upload_img')->name('Course_dore_cat_upload_img');
    Route::get('/Course_dore_cat_active','CourseDoreCatController@Course_dore_cat_active')->name('Course_dore_cat_active');
    Route::get('/Course_dore_cat_delete','CourseDoreCatController@Course_dore_cat_delete')->name('Course_dore_cat_delete');
    Route::post('/Course_dore_cat_store','CourseDoreCatController@Course_dore_cat_store')->name('Course_dore_cat_store');
    Route::post('/Course_dore_cat_update/{id}','CourseDoreCatController@Course_dore_cat_update')->name('Course_dore_cat_update');


    Route::get('/Course_dore_cat_sub/{id}/{name}','CourseDoreCatController@Course_dore_cat_sub')->name('Course_dore_cat_sub_index');
    Route::get('/Course_dore_cat_sub/create/{id}/{name}' , 'CourseDoreCatController@Course_dore_cat_sub_create')->name('Course_dore_cat_sub_create');
    Route::get('/Course_dore_cat_sub_active','CourseDoreCatController@Course_dore_cat_sub_active')->name('Course_dore_cat_sub_active');
    Route::get('/Course_dore_cat_sub_delete','CourseDoreCatController@Course_dore_cat_sub_delete')->name('Course_dore_cat_sub_delete');
    Route::post('/Course_dore_cat_sub_store/{id}/{name}','CourseDoreCatController@Course_dore_cat_sub_store')->name('Course_dore_cat_sub_store');

    Route::get('/Course_dore_cat_gallery/{id}/{name}','CourseDoreCatController@Course_dore_cat_gallery')->name('Course_dore_cat_gallery_index');
    Route::get('/Course_dore_cat_gallery/create/{id}/{name}' , 'CourseDoreCatController@Course_dore_cat_gallery_create')->name('Course_dore_cat_gallery_create');
    Route::get('/Course_dore_cat_gallery_active','CourseDoreCatController@Course_dore_cat_gallery_active')->name('Course_dore_cat_gallery_active');
    Route::get('/Course_dore_cat_gallery_delete','CourseDoreCatController@Course_dore_cat_gallery_delete')->name('Course_dore_cat_gallery_delete');
    Route::post('/Course_dore_cat_gallery_store/{id}/{name}','CourseDoreCatController@Course_dore_cat_gallery_store')->name('Course_dore_cat_gallery_store');
    Route::get('/Course_dore_cat_gallery_img','CourseDoreCatController@Course_dore_cat_gallery_img')->name('Course_dore_cat_gallery_img');


    Route::get('/Course_dore_cat_question/{id}/{name}','CourseDoreCatController@Course_dore_cat_question')->name('Course_dore_cat_question_index');
    Route::get('/Course_dore_cat_question/create/{id}/{name}' , 'CourseDoreCatController@Course_dore_cat_question_create')->name('Course_dore_cat_question_create');
    Route::get('/Course_dore_cat_question_active','CourseDoreCatController@Course_dore_cat_question_active')->name('Course_dore_cat_question_active');
    Route::get('/Course_dore_cat_question_delete','CourseDoreCatController@Course_dore_cat_question_delete')->name('Course_dore_cat_question_delete');
    Route::post('/Course_dore_cat_question_store/{id}/{name}','CourseDoreCatController@Course_dore_cat_question_store')->name('Course_dore_cat_question_store');




    /**
     * Admin Panel Course One Day
     */
    Route::get('/Course_one_day' , 'CourseOneDayController@Course_one_day_index')->name('Course_one_day_index');
    Route::get('/CourseOneDay/create' , 'CourseOneDayController@Course_one_day_create')->name('Course_one_day_create');
    Route::get('/CourseOneDay/edit/{id}' , 'CourseOneDayController@Course_one_day_edit')->name('Course_one_day_edit');
    Route::get('/CourseOneDay_upload_img','CourseOneDayController@Course_one_day_upload_img')->name('Course_one_day_upload_img');
    Route::get('/CourseOneDay_active','CourseOneDayController@Course_one_day_active')->name('Course_one_day_active');
    Route::get('/CourseOneDay_delete','CourseOneDayController@Course_one_day_delete')->name('Course_one_day_delete');
    Route::post('/CourseOneDay_store','CourseOneDayController@Course_one_day_store')->name('Course_one_day_store');
    Route::post('/CourseOneDay_update/{id}','CourseOneDayController@Course_one_day_update')->name('Course_one_day_update');


    Route::get('/CourseOneDay_sub/{id}/{name}','CourseOneDayController@Course_one_day_sub_index')->name('Course_one_day_sub_index');
    Route::get('/CourseOneDay_sub/create/{id}/{name}' , 'CourseOneDayController@Course_one_day_sub_create')->name('Course_one_day_sub_create');
    Route::get('/CourseOneDay_sub_active','CourseOneDayController@Course_one_day_sub_active')->name('Course_one_day_sub_active');
    Route::get('/CourseOneDay_sub_delete','CourseOneDayController@Course_one_day_sub_delete')->name('Course_one_day_sub_delete');
    Route::post('/CourseOneDay_sub_store/{id}/{name}','CourseOneDayController@Course_one_day_sub_store')->name('Course_one_day_sub_store');

    Route::get('/CourseOneDay_gallery/{id}/{name}','CourseOneDayController@Course_one_day_gallery_index')->name('Course_one_day_gallery_index');
    Route::get('/CourseOneDay_gallery/create/{id}/{name}' , 'CourseOneDayController@Course_one_day_gallery_create')->name('Course_one_day_gallery_create');
    Route::get('/CourseOneDay_gallery_active','CourseOneDayController@Course_one_day_gallery_active')->name('Course_one_day_gallery_active');
    Route::get('/CourseOneDay_gallery_delete','CourseOneDayController@Course_one_day_gallery_delete')->name('Course_one_day_gallery_delete');
    Route::post('/CourseOneDay_gallery_store/{id}/{name}','CourseOneDayController@Course_one_day_gallery_store')->name('Course_one_day_gallery_store');
    Route::get('/CourseOneDay_gallery_img','CourseOneDayController@Course_one_day_gallery_img')->name('Course_one_day_gallery_img');


    Route::get('/CourseOneDay_question/{id}/{name}','CourseOneDayController@Course_one_day_question_index')->name('Course_one_day_question_index');
    Route::get('/CourseOneDay_question/create/{id}/{name}' , 'CourseOneDayController@Course_one_day_question_create')->name('Course_one_day_question_create');
    Route::get('/CourseOneDay_question_active','CourseOneDayController@Course_one_day_question_active')->name('Course_one_day_question_active');
    Route::get('/CourseOneDay_question_delete','CourseOneDayController@Course_one_day_question_delete')->name('Course_one_day_question_delete');
    Route::post('/CourseOneDay_question_store/{id}/{name}','CourseOneDayController@Course_one_day_question_store')->name('Course_one_day_question_store');







    /**
     * Admin Panel Products
     */
    Route::get('/Product' , 'ProductController@Product_index')->name('Product_index');
    Route::get('/Product/create' , 'ProductController@Product_create')->name('Product_create');
    Route::get('/Product/edit/{id}' , 'ProductController@Product_edit')->name('Product_edit');
    Route::get('/Product_upload_img','ProductController@Product_upload_img')->name('Product_upload_img');
    Route::get('/Product_active','ProductController@Product_active')->name('Product_active');
    Route::get('/Product_delete','ProductController@Product_delete')->name('Product_delete');
    Route::post('/Product_store','ProductController@Product_store')->name('Product_store');
    Route::post('/Product_update/{id}','ProductController@Product_update')->name('Product_update');


    Route::get('/Product_cat','ProductController@Product_cat_index')->name('Product_cat_index');
    Route::post('/Product_cat/store','ProductController@Product_cat_store')->name('Product_cat_store');
    Route::get('/Product_cat/create' , 'ProductController@Product_cat_create')->name('Product_cat_create');
    Route::get('/Product_cat/active','ProductController@Product_cat_active')->name('Product_cat_active');
    Route::get('/Product_cat/delete','ProductController@Product_cat_delete')->name('Product_cat_delete');

    Route::get('/Product_tag','ProductController@Product_tag_index')->name('Product_tag_index');
    Route::post('/Product_tag/store','ProductController@Product_tag_store')->name('Product_tag_store');
    Route::get('/Product_tag/create' , 'ProductController@Product_tag_create')->name('Product_tag_create');
    Route::get('/Product_tag/active','ProductController@Product_tag_active')->name('Product_tag_active');
    Route::get('/Product_tag/delete','ProductController@Product_tag_delete')->name('Product_tag_delete');

    Route::get('/Product_gallery/{id}/{name}','ProductController@Product_gallery_index')->name('Product_gallery_index');
    Route::get('/Product_gallery/create/{id}/{name}' , 'ProductController@Product_gallery_create')->name('Product_gallery_create');
    Route::get('/Product_gallery_active','ProductController@Product_gallery_active')->name('Product_gallery_active');
    Route::get('/Product_gallery_delete','ProductController@Product_gallery_delete')->name('Product_gallery_delete');
    Route::post('/Product_gallery_store/{id}/{name}','ProductController@Product_gallery_store')->name('Product_gallery_store');
    Route::get('/Product_gallery_img','ProductController@Product_gallery_img')->name('Product_gallery_img');


    /**
     * Admin Panel News Route
     */


    Route::get('/News' , 'NewsController@index')->name('News_index');
    Route::get('/News/create' , 'NewsController@create')->name('News_create');
    Route::get('/News/edit/{id}' , 'NewsController@edit')->name('News_edit');
    Route::get('/News_upload_img','NewsController@upload_img')->name('News_upload_img');
    Route::get('/News_active','NewsController@active')->name('News_active');
    Route::get('/News_delete','NewsController@delete')->name('News_delete');
    Route::post('/News_store','NewsController@store')->name('News_store');
    Route::post('/News_update/{id}','NewsController@update')->name('News_update');
    Route::get('/News_important/{id}/{val}','NewsController@News_important')->name('News_important');




    /**
     * Admin Panel About Route
     */


    Route::get('/About_People' , 'AboutController@index')->name('About_People_index');
    Route::get('/About_People/create' , 'AboutController@create')->name('About_People_create');
    Route::get('/About_People/edit/{id}' , 'AboutController@edit')->name('About_People_edit');
    Route::get('/About_People_upload_img','AboutController@upload_img')->name('About_People_upload_img');
    Route::get('/About_People_active','AboutController@active')->name('About_People_active');
    Route::get('/About_People_delete','AboutController@delete')->name('About_People_delete');
    Route::post('/About_People_store','AboutController@store')->name('About_People_store');
    Route::post('/About_People_update/{id}','AboutController@update')->name('About_People_update');

    Route::get('/About_Company_edit','AboutController@edit_company')->name('About_Company_edit');
    Route::post('/About_Company_update','AboutController@update_company')->name('About_Company_update');





    /**
     * Admin Panel Question Route
     */


    Route::get('/Question' , 'QuestionController@index')->name('Question_index');
    Route::get('/Question/create' , 'QuestionController@create')->name('Question_create');
    Route::get('/Question_delete','QuestionController@delete')->name('Question_delete');
    Route::post('/Question_store','QuestionController@store')->name('Question_store');

    /**
     * Admin Panel Site Setting Route
     */
    Route::get('/Site_setting' , 'SiteSettingController@index')->name('Site_setting_index');
    Route::post('/Site_setting_update' , 'SiteSettingController@update')->name('Site_setting_update');

    /**
     * Admin Panel Site Setting Route
     */
    Route::get('/Site_setting' , 'SiteSettingController@index')->name('Site_setting_index');

    /**
     * Admin Panel Contact us form Route
     */
    Route::get('/Contact_form' , 'ContactUsFormController@index')->name('Contact_form');
    /**


    /**
     * Admin Panel error
     */
    Route::get('/error_403' , 'ErrorController@error_403')->name('error.403');





    /**
     * Admin Panel Video Route
     */


    Route::get('/Video' , 'VideoController@index')->name('Video_index');
    Route::get('/Video/create' , 'VideoController@create')->name('Video_create');
    Route::get('/Video/edit/{id}' , 'VideoController@edit')->name('Video_edit');
    Route::get('/Video_upload_img','VideoController@upload_img')->name('Video_upload_img');
    Route::get('/Video_active','VideoController@active')->name('Video_active');
    Route::get('/Video_delete','VideoController@delete')->name('Video_delete');
    Route::post('/Video_store','VideoController@store')->name('Video_store');
    Route::post('/Video_update/{id}','VideoController@update')->name('Video_update');


    Route::get('/Video_cat' , 'VideoController@index_cat')->name('Video_index_cat');
    Route::get('/Video_cat/create' , 'VideoController@create_cat')->name('Video_create_cat');
    Route::post('/Video_cat_store','VideoController@store_cat')->name('Video_store_cat');
    Route::get('/Video_cat_delete','VideoController@delete_cat')->name('Video_delete_cat');

    Route::get('/Video_cat_sub' , 'VideoController@index_cat_sub')->name('Video_index_cat_sub');
    Route::get('/Video_cat_sub/create' , 'VideoController@create_cat_sub')->name('Video_create_cat_sub');
    Route::post('/Video_cat_sub_store','VideoController@store_cat_sub')->name('Video_store_cat_sub');
    Route::get('/Video_cat_sub_delete','VideoController@delete_cat_sub')->name('Video_delete_cat_sub');



    /**
     * Admin Panel Video Route
     */


    Route::get('/Article' , 'ArticleController@index')->name('Article_index');
    Route::get('/Article/create' , 'ArticleController@create')->name('Article_create');
    Route::get('/Article/edit/{id}' , 'ArticleController@edit')->name('Article_edit');
    Route::get('/Article_upload_img','ArticleController@upload_img')->name('Article_upload_img');
    Route::get('/Article_active','ArticleController@active')->name('Article_active');
    Route::get('/Article_delete','ArticleController@delete')->name('Article_delete');
    Route::post('/Article_store','ArticleController@store')->name('Article_store');
    Route::post('/Article_update/{id}','ArticleController@update')->name('Article_update');
    Route::get('/Article_important/{id}/{val}','ArticleController@Article_important')->name('Article_important');



    Route::get('/Article_cat' , 'ArticleController@index_cat')->name('Article_index_cat');
    Route::get('/Article_cat/create' , 'ArticleController@create_cat')->name('Article_create_cat');
    Route::post('/Article_cat_store','ArticleController@store_cat')->name('Article_store_cat');
    Route::get('/Article_cat_delete','ArticleController@delete_cat')->name('Article_delete_cat');



    /**
     * Admin Panel Customer Advise Route
     */

    Route::get('/Customer_Advise' , 'CustomerAdviseController@index')->name('Customer_Advise_index');
    Route::get('/Customer_Advise/create' , 'CustomerAdviseController@create')->name('Customer_Advise_create');
    Route::post('/Customer_Advise_store','CustomerAdviseController@store')->name('Customer_Advise_store');
    Route::get('/Customer_Advise_delete','CustomerAdviseController@delete')->name('Customer_Advise_delete');
    Route::get('/Customer_Advise_upload_img','CustomerAdviseController@upload_img')->name('Customer_Advise_upload_img');

    /**
     * Admin Panel NLP Result Route
     */

    Route::get('/NLP_Result' , 'NLPResultController@index')->name('NLP_Result_index');
    Route::get('/NLP_Result/create' , 'NLPResultController@create')->name('NLP_Result_create');
    Route::post('/NLP_Result_store','NLPResultController@store')->name('NLP_Result_store');
    Route::get('/NLP_Result_delete','NLPResultController@delete')->name('NLP_Result_delete');
    Route::get('/NLP_Result_upload_img','NLPResultController@upload_img')->name('NLP_Result_upload_img');



    Route::get('/User_registration_dore' , 'UserDoreRegisterController@index')->name('User_registration_dore');



});



/**
 * Site NLP Route
 */

Route::group(['namespace' => 'SiteController'] , function () {

    Route::get('/', 'MainController@index')->name('site_index');
    Route::get('/Article_list', 'MainController@Article_list')->name('Article_list');
    Route::get('/Article_list_by_cat/{cat_id}', 'MainController@Article_list_by_cat')->name('Article_list_by_cat');
    Route::get('/Article_single/{id}', 'MainController@Article_single')->name('Article_single');

    Route::get('/Course_list', 'MainController@course_list')->name('course_list');
    Route::get('/Video_list', 'MainController@Video_list')->name('Video_list');
    Route::get('/Video_list_by_cat_sub_id/{id}', 'MainController@Video_list_by_cat_sub_id')->name('Video_list_by_cat_sub_id');
    Route::get('/Video_cat_list_by_id/{id}', 'MainController@Video_cat_list_by_id')->name('Video_cat_list_by_id');
    Route::get('/Video_single/{id}', 'MainController@Video_single')->name('Video_single');
    Route::get('/Contact_us', 'MainController@Contact_us')->name('Contact_us');
    Route::get('/Question_list', 'MainController@Question_list')->name('Question_list');
    Route::get('/About_us', 'MainController@About_us')->name('About_us');
    Route::get('/News_list', 'MainController@News_list')->name('News_list');
    Route::get('/Product_list', 'MainController@Product_list')->name('Product_list');
    Route::get('/course_dore_cat_single/{id}', 'MainController@course_dore_cat_single')->name('course_dore_cat_single');
    Route::get('/course_one_day_single/{id}', 'MainController@course_one_day_single')->name('course_one_day_single');
    Route::get('/product_single/{id}', 'MainController@product_single')->name('site_product_single');
    Route::get('/News_single/{id}', 'MainController@News_single')->name('News_single');
    Route::get('/site_blog_single/{id}', 'MainController@site_blog_single')->name('site_blog_single');

    Route::post('/Contact_us_form', 'MainController@Contact_us_form')->name('Contact_us_form');


});