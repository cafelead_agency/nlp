<?php

namespace App\Http\Middleware;

use App\Model\Site\Users;
use Carbon\Carbon;
use Closure;

class check_token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $db = Users::where('token' , $request->token )->first();
        if ($db){
            if($db->expire_token > Carbon::now()->timestamp){
                return $next($request);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "مدت زمان توکن تمام شده است" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "توکن با id مورد نظر وجود ندارد" , "status" => 403] ], 403);
        }
    }
}
