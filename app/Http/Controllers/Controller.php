<?php

namespace App\Http\Controllers;

use App\Model\admin_panel\AdminLog;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public static function session(){

        if ( session()->has('admin_user') ){
            $id = session()->get('admin_user.id');
            $firstname = session()->get('admin_user.firstname');
            $lastname = session()->get('admin_user.lastname');
            $email = session()->get('admin_user.email');
            $tel = session()->get('admin_user.tel');
            $avatar = session()->get('admin_user.avatar');
            $fullname = $firstname . ' ' . $lastname;
        }else{
            echo '<script>window.location.replace("/mgmt_portal/admin_users_login")</script>';
            /*header('Location: /login/user');
            die();*/
        }
    }

    public static function check_permission($sub_permission_id){

        $flag = 0;
        $user_permissions = DB::table('sub_per_users')->
        where('admin_user_id' ,session('admin_user.id'))->get();
        foreach ($user_permissions as $per) {
            if ($per->sub_permission_id == $sub_permission_id) {
                $flag = 1;
            }
        }
        return $flag;

    }

    public static function log_admin($Tbl,$row_id,$Act){
        AdminLog::create([
            'table_name'=>$Tbl,
            'row_id' =>$row_id,
            'action'=>$Act,
            'action_by'=>session('admin_user.id')
        ]);

    }
}
