<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/25/2019
 * Time: 12:17 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Model\Site\Users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use SoapClient;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{

    public function sendMessage($smsphone,$smstext){
        $sms_client = new SoapClient('http://api.payamak-panel.com/post/send.asmx?wsdl', array('encoding'=>'UTF-8'));
        $parameters['username'] = '9355073320';
        $parameters['password'] = 'Ah@5073320';
        $parameters['to'] = $smsphone;
        $parameters['from'] = '30004657500500';
        $parameters['text'] =$smstext;
        $parameters['isflash'] =false;
        $sms_client->SendSimpleSMS2($parameters)->SendSimpleSMS2Result;
    }

    public $expiration_date_period = 30 ; // in minutes



    public function sms(){
//        $sms_client = new SoapClient('http://api.payamak-panel.com/post/send.asmx?wsdl', array('encoding'=>'UTF-8'));
//        $parameters['username'] = '9355073320';
//        $parameters['password'] = 'Ah@5073320';
//        $parameters['to'] = '9025882272';
//        $parameters['from'] = '30004657500500';
//        $parameters['text'] ="salam";
//        $parameters['isflash'] =false;
//        echo $sms_client->SendSimpleSMS2($parameters)->SendSimpleSMS2Result;
        $this->sendMessage('9025882272' , "salam");
    }

    public function user_info(Request $request){
        $db = Users::where('token' , $request->token )->first();
        return response()->json(["user_data" => ["tel" => $db->tel , "email" => $db->email , "firstname" => $db->firstname ,"lastname" => $db->lastname
            ,"address" => $db->address ,"comment" => $db->comment] , "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
    }

    public function update_user_info(Request $request){
        $db = Users::where('token' , $request->token )->first();

                if ($request->has('email')) {
                    $db->email = $request->get('email');
                }
                if ($request->has('address')) {
                    $db->address = $request->get('address');
                }
                if ($request->has('firstname')) {
                    $db->firstname = $request->get('firstname');
                }
                if ($request->has('lastname')) {
                    $db->lastname = $request->get('lastname');
                }
                if ($request->has('comment')) {
                    $db->comment = $request->get('comment');
                }
                $db->save();
                return response()->json(["user_data" => ["tel" => $db->tel , "email" => $db->email , "firstname" => $db->firstname ,"lastname" => $db->lastname
                    ,"address" => $db->address ,"comment" => $db->comment] , "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
    }



    public function user_login(Request $request){
        $db = Users::where('tel' , $request->tel )->first();
        if ($db){
            $new_password = rand(10000,99999);
            $db->password = Hash::make($new_password);
            $db->save();
            $this->sendMessage($request->tel , "رمز عبور یک بار مصرف شما : $new_password");

            return response()->json(["user_data" => null ,
                "msg" => ["message" => "رمز عبور جدید برای شماره تماس کاربر ارسال شد" , "status" => 200] ], 200);
        }else{
            $new_password = rand(10000,99999);
            $token = Hash::make($request->tel);
            $expire_token = Carbon::now()->addMinutes($this->expiration_date_period)->timestamp;
            $db = Users::create([
                'tel'=>$request->tel,
                'password'=>Hash::make($new_password),
                'token'=>$token,
                'expire_token'=>$expire_token,
            ]);
            $db->save();
            $this->sendMessage($db->tel , "رمز عبور یک بار مصرف شما : $new_password");
            return response()->json(["user_data" => ["tel" => $db->tel , "token" => $db->token] ,
                "msg" => ["message" => "کاربر در سیستم ثبت شد" , "status" => 200] ], 200);




        }
    }

    public function user_do_login(Request $request){

        $check_password = 0;
        $db = Users::where('tel' , $request->tel )->first();
        if ($db){
            if (Hash::check($request->password, $db->password)) {
                $check_password = 1;
            }
            if ($check_password == 1){
                $expire_token = Carbon::now()->addMinutes($this->expiration_date_period)->timestamp;
                $token = Hash::make($request->tel);
                $db->token = $token;
                $db->expire_token = $expire_token;
                $db->save();
                return response()->json(["user_data" => ["tel" => $db->tel , "token" => $db->token] , "msg" => ["message" => "ورود با موفقیت انجام شد" , "status" => 200] ], 200);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "نام کاربری و رمز عبور مطابقت ندارد" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "چنین کاربری در سیستم وجود ندارد" , "status" => 403] ], 403);

        }
    }



}