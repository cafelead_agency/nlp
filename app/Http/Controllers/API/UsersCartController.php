<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/25/2019
 * Time: 12:17 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Model\Site\Users;
use App\Model\Site\UsersCart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;

class UsersCartController extends Controller
{

    public $expiration_date_period = 30 ; // in minutes

    public function user_cart_info(Request $request){
        $db = Users::where('token' , $request->token )->first();
        if ($db){
            if($db->expire_token > Carbon::now()->timestamp){
                $db_cart = UsersCart::where('user_id' , $db->id)->where('is_success' , 0)->get();
                return response()->json(["user_data" => ["tel" => $db->tel] , "user_cart_data" => $db_cart ,
                    "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "مدت زمان توکن تمام شده است" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "توکن با id مورد نظر وجود ندارد" , "status" => 403] ], 403);
        }
    }


    public function add_new_user_cart(Request $request){
        $db = Users::where('token' , $request->token )->first();
        if ($db){
            if($db->expire_token > Carbon::now()->timestamp){
                $db_cart = UsersCart::create([
                    'cart_detail'=>$request->data,
                    'user_id'=>$db->id,

                ]);
                $db_cart->save();

                return response()->json(["user_data" => ["tel" => $db->tel] , "user_cart_data" => $db_cart ,
                    "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "مدت زمان توکن تمام شده است" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "توکن با id مورد نظر وجود ندارد" , "status" => 403] ], 403);
        }

    }

    public function remove_user_cart(Request $request){
        $db = Users::where('token' , $request->token )->first();
        if ($db){
            if($db->expire_token > Carbon::now()->timestamp){
                UsersCart::destroy($request->cart_id);


                return response()->json(["user_data" => ["tel" => $db->tel] , "user_cart_id" => $request->cart_id ,
                    "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "مدت زمان توکن تمام شده است" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "توکن با id مورد نظر وجود ندارد" , "status" => 403] ], 403);
        }
    }

    public function buy_user_cart(Request $request){
        $db = Users::where('token' , $request->token )->first();
        if ($db){
            if($db->expire_token > Carbon::now()->timestamp){

                return 'خرید انجام شد';
                return response()->json(["user_data" => ["tel" => $db->tel] , "user_cart_id" => $request->cart_id ,
                    "msg" => ["message" => "هنوز مدت زمان توکن باقی است" , "status" => 200] ], 200);
            }else{
                return response()->json(["user_data" => null , "msg" => ["message" => "مدت زمان توکن تمام شده است" , "status" => 403] ], 200);
            }
        }else{
            return response()->json(["user_data" => null , "msg" => ["message" => "توکن با id مورد نظر وجود ندارد" , "status" => 403] ], 403);
        }

    }



}