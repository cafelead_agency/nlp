<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:27 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\admin_panel\AdminUser;
use App\Model\admin_panel\Permission;
use Illuminate\Http\Request;


class PermissionController extends Controller
{
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user = AdminUser::where('id' , $id)->first();
        return view('admin_panel.roles.defin_permision' ,compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request,$id)
    {
        $this->validate($request , [
            'name' => 'required'
        ]);

        $permission = Permission::create([
            'name'=>$request['name'],
            'description'=>$request['description']
        ]);


        return redirect()->route('permission_list',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}