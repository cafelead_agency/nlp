<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\Article;
use App\Model\Site\Article_cat;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use App\Model\Site\CustomerAdvise;
use App\Model\Site\News;
use App\Model\Site\NLP_Result;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use App\Model\Site\Video;
use App\Model\Site\Video_cat;
use App\Model\Site\Video_cat_sub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NLPResultController extends Controller
{
    public function index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = NLP_Result::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.nlp_result.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){



            return view('admin_panel.nlp_result.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store(Request $request)
    {
        Controller::session();
        $this->validate($request, [
            'name' => 'required',
        ]);

        $db = NLP_Result::create([
            'name'=>$request['name'],

        ]);
        $db->save();

        return redirect()->route('NLP_Result_index');
    }



    public function upload_img(Request $request){
        $id = $request->id;
        $db = NLP_Result::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_NLP_Result',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }





    public function delete(Request $request){
        $id = $request->id;
        $db = NLP_Result::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_NLP_Result',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }







}