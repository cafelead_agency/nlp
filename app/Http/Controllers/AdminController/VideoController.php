<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use App\Model\Site\News;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use App\Model\Site\Video;
use App\Model\Site\Video_cat;
use App\Model\Site\Video_cat_sub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VideoController extends Controller
{
    public function index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Video::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Video.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){

            $cat = Video_cat::where('is_delete', '=', 0)->get();
            $cat_sub = Video_cat_sub::where('is_delete', '=', 0)->get();


            return view('admin_panel.Video.create' , compact('cat' , 'cat_sub'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store(Request $request)
    {
        Controller::session();
        $this->validate($request, [
            'name' => 'required',
            'text' => 'required',
        ]);


        if ($request->video){
            $video = $request->file('video');
            $new_name = date('Y-M-D-H:i:s') . '.' . $video->getClientOriginalExtension();
            if($video->getClientOriginalExtension() != 'mp4' && $video->getClientOriginalExtension() != 'avi' &&
                $video->getClientOriginalExtension() != 'MP4' && $video->getClientOriginalExtension() != 'AVI' )
            {
                return Redirect::back()->withErrors(['فرمت ویدیو باید avi یا mp4 باشد']);
            }else{
                $video->move('/home/cafeir/public_html/nlp/video_upload/', $new_name);
            }
        }






        $db = Video::create([
            'name'=>$request['name'],
            'aparat'=>$request['aparat'],
            'video'=>$request['video'],
            'text'=>$request['text'],
            'cat_id'=>$request['cat_id'],
            'cat_sub_id'=>$request['cat_sub_id'],
        ]);
        $db->save();

        return redirect()->route('Video_index');
    }



    public function edit($id){
        Controller::session();
        $edit = Controller::check_permission(3 );


        if($edit == 1){
            $cat = Video_cat::where('is_delete', '=', 0)->get();
            $cat_sub = Video_cat_sub::where('is_delete', '=', 0)->get();
            $db = Video::where('id', $id)->first();
            return view('admin_panel.Video.edit' ,  compact('db' , 'cat','cat_sub'));
        } else {

            return view('error.pages-403');
        }
    }


    public function update(Request $request , $id)
    {
        $db = Video::where('id' , $id)->first();

        if ($request->has('name')) {
            $db->name = $request->get('name');
        }
        if ($request->has('aparat')) {
            $db->aparat = $request->get('aparat');
        }
        if ($request->has('video')) {
            $video = $request->file('video');
            $new_name = date('Y-M-D-H:i:s').'.' . $video->getClientOriginalExtension();
            if($video->getClientOriginalExtension() != 'mp4' && $video->getClientOriginalExtension() != 'avi' &&
                $video->getClientOriginalExtension() != 'MP4' && $video->getClientOriginalExtension() != 'AVI' )
            {
                return Redirect::back()->withErrors(['فرمت ویدیو باید avi یا mp4 باشد']);

            }else{
                $video->move('/home/cafeir/public_html/nlp/video_upload/', $new_name);
            }
            $db->video = '/video_upload/'.$new_name;
        }
        if ($request->has('text')) {
            $db->text = $request->get('text');
        }
        if ($request->has('cat_id')) {
            $db->cat_id = $request->get('cat_id');
        }
        if ($request->has('cat_sub_id')) {
            $db->cat_sub_id = $request->get('cat_sub_id');
        }
        $db->save();
        Controller::log_admin('tbl_Video',$db->id,'update');
        return redirect()->route('Video_index');
    }




    public function upload_img(Request $request){
        $id = $request->id;
        $db = Video::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_Video',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function active(Request $request){

        $id = $request->id;
        $db = Video::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_Video',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function delete(Request $request){
        $id = $request->id;
        $db = Video::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Video',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }







    public function index_cat(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Video_cat::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Video_cat.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create_cat(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Video_cat.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store_cat(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $db = Video_cat::create([
            'name'=>$request['name'],
        ]);
        $db->save();

        return redirect()->route('Video_index_cat');
    }

    public function delete_cat(Request $request){
        $id = $request->id;
        $db = Video_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Video_cat',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



    public function index_cat_sub(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Video_cat_sub::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Video_cat_sub.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create_cat_sub(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            $cat = Video_cat::where('is_delete', '=', 0)->get();
            return view('admin_panel.Video_cat_sub.create' , compact('cat'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store_cat_sub(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $db = Video_cat_sub::create([
            'name'=>$request['name'],
            'cat_id'=>$request['cat_id'],
        ]);
        $db->save();

        return redirect()->route('Video_index_cat_sub');
    }

    public function delete_cat_sub(Request $request){
        $id = $request->id;
        $db = Video_cat_sub::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Video_cat_sub',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



}