<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/10/2019
 * Time: 8:40 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\admin_panel\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function index(){
        Controller::session();

        return view('admin_panel.pages.dashboard');

    }


    public function login(){
        return view('admin_panel.pages.login');

    }

    public function dologin(Request $request){
        $check_password = 0;
        $user = AdminUser::where('email' ,$request->username)->first();

        if ($user){
            if (Hash::check($request->password, $user->password)) {
                $check_password = 1;
            }

            if($user &&  $check_password == 1 && $user->is_delete == 0 && $user->is_active == 1)
            {
                session(['admin_user.id' => $user->id,
                    'admin_user.firstname' => $user->firstname,
                    'admin_user.lastname' => $user->lastname,
                    'admin_user.email' => $user->email,
                    'admin_user.tel' => $user->tel,
                    'admin_user.img' => $user->img]);


                return redirect()->route('admin_dashboard');
            }else{
                return redirect()->back()->withErrors(['نام کاربری یا رمز عبور اشتباه است']);
            }
        }else{
            return redirect()->back()->withErrors(['نام کاربری یا رمز عبور اشتباه است']);

        }
    }

    public function logout(){
        session()->flush();
        return redirect()->route('admin_users_login');

    }
}