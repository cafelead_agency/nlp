<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/11/2019
 * Time: 9:29 AM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\admin_panel\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUsersController extends Controller
{
    public function index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $changepassword_permission = Controller::check_permission(7 );

        if($show == 1){
            $admin_users = AdminUser::where('is_delete', '=', 0)->where('id','!=',1)->latest()->paginate(10);
            return view('admin_panel.admin_users.index', compact('admin_users','pic_permission','delete_permission','active_permission','changepassword_permission'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function create(){
        Controller::session();
        $show = Controller::check_permission(2);

        if($show == 1){

            return view('admin_panel.admin_users.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store(Request $request){
        Controller::session();
        $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:tbl_admin_users,tel|numeric',
            'email' => 'required|email|unique:tbl_admin_users',
            'password' => 'required'
        ]);

        $user = AdminUser::create([
            'firstname'=>$request['first_name'],
            'lastname'=>$request['last_name'],
            'email'=>$request['email'],
            'tel'=>$request['phone'],
            'password'=>Hash::make($request->password)
        ]);
        $user->save();

        $new_user = AdminUser::where('email',$request->email)->first();

        Controller::log_admin('tbl_admin_users',$new_user->id,'create_user');

        return redirect()->route('admin_users_index');
    }


    public function edit($id){
        Controller::session();
        $edit = Controller::check_permission(3 );

        if($edit == 1){
            $db = AdminUser::where('id', $id)->first();
            return view('admin_panel.admin_users.edit' ,  compact('db'));
        } else {

            return view('error.pages-403');
        }
    }


    public function update(Request $request , $id)
    {
        $db = AdminUser::where('id' , $id)->first();

        if ($request->has('first_name')) {
            $db->firstname = $request->get('first_name');
        }
        if ($request->has('last_name')) {
            $db->lastname = $request->get('last_name');
        }
        if ($request->has('phone')) {
            $db->tel = $request->get('phone');
        }
        if ($request->has('email')) {
            $db->email = $request->get('email');
        }


        $db->save();

        Controller::log_admin('tbl_admin_users',$db->id,'admin_users_update');

        return redirect()->route('admin_users_index');
    }


    public function admin_user_upload_img(Request $request){
        $id = $request->id;
        $db = AdminUser::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_admin_users',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function admin_user_is_active(Request $request){

        $id = $request->id;
        $db = AdminUser::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_admin_users',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function admin_user_is_delete(Request $request){
        $id = $request->id;
        $db = AdminUser::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_admin_users',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }

    public function admin_user_change_password(Request $request){
        $id = $request->id;
        $db = AdminUser::where('id', $id)->first();
        if($db)
        {
            $db->password = Hash::make($request->pass_value);
            $db->save();
            Controller::log_admin('tbl_admin_users',$id,"change password");
            $msg = 'با موفقیت تغییر یافت';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'user_admin_change_password';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }
}