<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:24 PM
 */

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Model\admin_panel\AdminUser;
use App\Model\admin_panel\Permission;
use App\Model\admin_panel\SubPermission;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function index($id)
    {
        Controller::session();
        $role_p = Controller::check_permission(8 );

        if($role_p == 1){
            $per_id = array();
            $permission = array();
            $subs = array();
            $user = AdminUser::where('id', $id)->first();
            $user_permissions = AdminUser::with('permissions')->where('id', $id)->first();
            $user_subpermissions = AdminUser::with('subpermissions')->where('id', $id)->first();
            $subpermissions = $user_subpermissions->subpermissions;
            $permissions = $user_permissions->permissions;
            foreach ($permissions as $per){
                $per_id [] = $per->id;
            }
            $per_id_unique ['per']  = array_unique($per_id);

            foreach ($per_id_unique['per'] as $p_u){
                $subs[] = DB::table('sub_per_users')->join('sub_permissions','sub_per_users.sub_permission_id','=','sub_permissions.id')->where('sub_per_users.admin_user_id','=',$id)->where('sub_per_users.permission_id','=',$p_u)->select('sub_permissions.id','sub_permissions.name','sub_per_users.permission_id')->get();
                $permission[] = Permission::with('subpermissions')->where('id' , $p_u)->first();
            }
            return view('admin_panel.roles.list_accesses' ,  compact('user','permission','subs'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        Controller::session();
        $user = AdminUser::where('id' , $id)->first();
//        $user_= AdminUser::with('permissions','subpermissions')->where('id', $id)->first();
        $permissions = Permission::all();
        $subpermissions = SubPermission::all();
        return view('admin_panel.roles.define_role' , compact('user','permissions','subpermissions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {


        $user = AdminUser::where('id',$id)->first();

        if ($request->perm == 0)
        {
            return redirect()->route('error.403');
        }

        $permission = Permission::where('id',$request->perm)->first();
//        $permission = DB::table('sub_per_users')->where('admin_user_id',$id)->where('permission_id',$permission->id)->select('sub_per_users.id')->get();
        $user->permissions()->detach($permission->id);

        if($request->sub_per != null){
            foreach($request->sub_per as $sub) {

                DB::table('sub_per_users')->insert(
                    ['admin_user_id' => $id,
                        'permission_id' => $permission->id,
                        'sub_permission_id' => $sub]
                );

                $new_subpermission = DB::table('sub_per_users')->where('admin_user_id',$id)->where('permission_id',$permission->id)->where('sub_permission_id',$sub)->select('sub_per_users.id')->get();

                foreach ($new_subpermission as $new_sub) {
                    Controller::log_admin('sub_per_users', $new_sub->id, 'select_subpermission');
                }
            }
        }

        return redirect()->route('permission_list' ,$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id,$per_id)
    {
        $user = AdminUser::where('id',$user_id)->first();


        $permission = DB::table('sub_per_users')->where('admin_user_id',$user_id)->where('permission_id',$per_id)->select('sub_per_users.id')->get();

        foreach ($permission as $per){

            Controller::log_admin('sub_per_users',$per->id,'delete_permission');

        }
        $user->permissions()->detach($per_id);

        return redirect()->route('permission_list',$user_id);
    }
}