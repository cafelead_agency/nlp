<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:29 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\admin_panel\AdminUser;
use App\Model\admin_panel\SubPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user = AdminUser::where('id' , $id)->first();
        return view('admin_panel.roles.define_sub_permision' ,compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $id)
    {
        $this->validate($request , [
            'name' => 'required',
            'description' => 'required'
        ]);

        SubPermission::create([
            'name'=>$request['name']
        ]);

        return redirect()->route('permission_list',$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy()
    {

    }


    public function select_sub(Request $request){
        $selected_sub = array();
        $flag = 0;
        $total_sub = array();
        $sub_array = array();
        $sub_id_array = array();

        $user_subpermissions = DB::table('sub_per_users')->where('sub_per_users.permission_id','=',$request->permission)->where('sub_per_users.admin_user_id','=',$request->user_id)->select('sub_per_users.sub_permission_id')->get()->toArray();



        if($user_subpermissions != null) {


            foreach ($user_subpermissions as $user_sub) {



                $subpermissions = DB::table('sub_permissions')->where('permission_id','=',$request->permission)->get()->toArray();





                $subpermission = SubPermission::where('id', $user_sub->sub_permission_id)->get();


                foreach ($subpermission as $s) {
                    $selected_sub[] = array('id' => $s->id, 'name' => $s->name);
                }


            }
            foreach ($subpermissions as $sub) {
                $flag = 0;
                foreach ($user_subpermissions as $usub) {

                    if ($sub->id == $usub->sub_permission_id) {
                        $flag = 1;
                    }
                }
                if ($flag == 0) {
                    $total_sub[] = array('id' => $sub->id, 'name' => $sub->name);
                }

            }



        }else {



            $subpermissions = DB::table('sub_permissions')->where('permission_id','=',$request->permission)->get()->toArray();

            foreach ($subpermissions as $subper) {
                $sub_array[] = $subper;
            }
            foreach ($sub_array as $sub) {
                $s = SubPermission::where('id',$sub->id)->first();
                $total_sub[] = array('id' => $s->id, 'name' => $s->name);
            }
        }



        $data = array();
        $data['sub_selected']=$selected_sub;
        $data['sub_unselected']=$total_sub;
        $myObj['data'] = $data;
        $myObj['act'] = 'selectSubPermission';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



}