<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;

class ErrorController extends Controller
{
    public function error_403(){
        return view('admin_panel.pages.error.pages-403');
    }
}