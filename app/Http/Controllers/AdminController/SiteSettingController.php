<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/14/2019
 * Time: 9:59 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\Site_Setting;
use Illuminate\Http\Request;

class SiteSettingController extends Controller
{
    public function index(){

        Controller::session();
        $show = Controller::check_permission(1 );

        $db = Site_Setting::where('id', '=', 1)->first();


        if($show == 1) {
            return view('admin_panel.Site_setting.create', compact('db'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function update(Request $request)
    {
        $db = Site_Setting::where('id' , 1)->first();

        if ($request->has('primary_number')) {
            $db->primary_number = $request->get('primary_number');
        }
        if ($request->has('address')) {
            $db->address = $request->get('address');
        }
        if ($request->has('telegram')) {
            $db->telegram = $request->get('telegram');
        }
        if ($request->has('instagram')) {
            $db->instagram = $request->get('instagram');
        }

        if ($request->has('whatsapp')) {
            $db->whatsapp = $request->get('whatsapp');
        }
        if ($request->has('phone')) {
            $db->phone = $request->get('phone');
        }
        if ($request->has('f_title_nlp1')) {
            $db->f_title_nlp1 = $request->get('f_title_nlp1');
        }
        if ($request->has('f_text_nlp1')) {
            $db->f_text_nlp1 = $request->get('f_text_nlp1');
        }
        if ($request->has('f_title_nlp2')) {
            $db->f_title_nlp2 = $request->get('f_title_nlp2');
        }
        if ($request->has('f_text_nlp2')) {
            $db->f_text_nlp2 = $request->get('f_text_nlp2');
        }
        if ($request->has('f_title_nlp3')) {
            $db->f_title_nlp3 = $request->get('f_title_nlp3');
        }
        if ($request->has('f_text_nlp3')) {
            $db->f_text_nlp3 = $request->get('f_text_nlp3');
        }
        if ($request->has('f_title_nlp4')) {
            $db->f_title_nlp4 = $request->get('f_title_nlp4');
        }
        if ($request->has('f_text_nlp4')) {
            $db->f_text_nlp4 = $request->get('f_text_nlp4');
        }
        if ($request->has('f_title_nlp5')) {
            $db->f_title_nlp5 = $request->get('f_title_nlp5');
        }
        if ($request->has('f_text_nlp5')) {
            $db->f_text_nlp5 = $request->get('f_text_nlp5');
        }
        if ($request->has('f_title_nlp6')) {
            $db->f_title_nlp6 = $request->get('f_title_nlp6');
        }
        if ($request->has('f_title_nlp7')) {
            $db->f_title_nlp7 = $request->get('f_title_nlp7');
        }
        if ($request->has('f_text_nlp7')) {
            $db->f_text_nlp7 = $request->get('f_text_nlp7');
        }
        if ($request->has('f_title_nlp8')) {
            $db->f_title_nlp8 = $request->get('f_title_nlp8');
        }
        if ($request->has('f_text_nlp8')) {
            $db->f_text_nlp8 = $request->get('f_text_nlp8');
        }


        $db->save();

        Controller::log_admin('tbl_Site_setting',$db->id,'update');

        return redirect()->route('Site_setting_index');
    }
}