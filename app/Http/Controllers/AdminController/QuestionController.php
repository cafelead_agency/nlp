<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use App\Model\Site\News;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use App\Model\Site\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Question::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Question.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Question.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
            'text' => 'required',
        ]);

        $db = Question::create([
            'name'=>$request['name'],
            'text'=>$request['text'],
        ]);
        $db->save();

        return redirect()->route('Question_index');
    }





    public function delete(Request $request){
        $id = $request->id;
        $db = Question::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Question',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }













}