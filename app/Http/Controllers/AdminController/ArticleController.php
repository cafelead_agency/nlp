<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\Article;
use App\Model\Site\Article_cat;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use App\Model\Site\News;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use App\Model\Site\Video;
use App\Model\Site\Video_cat;
use App\Model\Site\Video_cat_sub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{
    public function index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Article::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Article.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){

            $cat = Article_cat::where('is_delete', '=', 0)->get();


            return view('admin_panel.Article.create' , compact('cat'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store(Request $request)
    {
        Controller::session();
        $this->validate($request, [
            'name' => 'required',
            'list' => 'required',
            'text' => 'required',
        ]);

        $db = Article::create([
            'name'=>$request['name'],
            'list'=>$request['list'],
            'text'=>$request['text'],
            'cat_id'=>$request['cat_id'],
        ]);
        $db->save();

        return redirect()->route('Article_index');
    }



    public function edit($id){
        Controller::session();
        $edit = Controller::check_permission(3 );


        if($edit == 1){
            $cat = Article_cat::where('is_delete', '=', 0)->get();
            $db = Article::where('id', $id)->first();
            return view('admin_panel.Article.edit' ,  compact('db' , 'cat'));
        } else {

            return view('error.pages-403');
        }
    }


    public function update(Request $request , $id)
    {
        $db = Article::where('id' , $id)->first();

        if ($request->has('name')) {
            $db->name = $request->get('name');
        }
        if ($request->has('list')) {
            $db->list = $request->get('list');
        }
        if ($request->has('text')) {
            $db->text = $request->get('text');
        }
        if ($request->has('cat_id')) {
            $db->cat_id = $request->get('cat_id');
        }
        $db->save();
        Controller::log_admin('tbl_Article',$db->id,'update');
        return redirect()->route('Article_index');
    }




    public function upload_img(Request $request){
        $id = $request->id;
        $db = Article::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_Article',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function active(Request $request){

        $id = $request->id;
        $db = Article::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_Article',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function delete(Request $request){
        $id = $request->id;
        $db = Article::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Article',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Article_important(Request $request){

        $id = $request->id;
        $db = Article::where('id', $id)->first();
        if($db)
        {
            $db->imprtant = $request->val;
            $db->save();
        }
        return redirect()->route('Article_index');


    }







    public function index_cat(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Article_cat::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Article_cat.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function create_cat(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Article_cat.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function store_cat(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $db = Article_cat::create([
            'name'=>$request['name'],
        ]);
        $db->save();

        return redirect()->route('Article_index_cat');
    }

    public function delete_cat(Request $request){
        $id = $request->id;
        $db = Article_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_Article_cat',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }






}