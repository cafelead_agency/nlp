<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use Illuminate\Http\Request;

class CourseDoreCatController extends Controller
{
    public function course_dore_cat_index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = course_dore_cat::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.course_dore_cat.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function course_dore_cat_create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){

            return view('admin_panel.course_dore_cat.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function Course_dore_cat_store(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
            'info' => 'required',
            'about' => 'required',
            'sweet' => 'required',
            'teacher' => 'required',

        ]);

        $db = course_dore_cat::create([
            'name'=>$request['name'],
            'info'=>$request['info'],
            'about'=>$request['about'],
            'sweet'=>$request['sweet'],
            'teacher'=>$request['teacher'],
        ]);
        $db->save();

        return redirect()->route('Course_dore_cat_index');
    }



    public function Course_dore_cat_edit($id){
        Controller::session();
        $edit = Controller::check_permission(3 );

        if($edit == 1){
            $db = course_dore_cat::where('id', $id)->first();
            return view('admin_panel.course_dore_cat.edit' ,  compact('db'));
        } else {

            return view('error.pages-403');
        }
    }


    public function Course_dore_cat_update(Request $request , $id)
    {
        $db = course_dore_cat::where('id' , $id)->first();

        if ($request->has('name')) {
            $db->name = $request->get('name');
        }
        if ($request->has('info')) {
            $db->info = $request->get('info');
        }
        if ($request->has('about')) {
            $db->about = $request->get('about');
        }
        if ($request->has('sweet')) {
            $db->sweet = $request->get('sweet');
        }
        if ($request->has('teacher')) {
            $db->teacher = $request->get('teacher');
        }


        $db->save();

        Controller::log_admin('tbl_course_dore_cat',$db->id,'update');

        return redirect()->route('Course_dore_cat_index');
    }




    public function Course_dore_cat_upload_img(Request $request){
        $id = $request->id;
        $db = course_dore_cat::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Course_dore_cat_active(Request $request){

        $id = $request->id;
        $db = course_dore_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Course_dore_cat_delete(Request $request){
        $id = $request->id;
        $db = course_dore_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



    public function Course_dore_cat_sub($id , $name){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Course_dore_cat_sub::where('course_dore_cat_id', $id)->where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.course_dore_cat_sub.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Course_dore_cat_sub_create($id , $name){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.course_dore_cat_sub.create' , compact('id' , 'name'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Course_dore_cat_sub_store(Request $request , $id , $name){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
            'expire' => 'required',
            'price' => 'required',


        ]);

        $db = Course_dore_cat_sub::create([
            'name'=>$request['name'],
            'expire'=>$request['expire'],
            'price'=>$request['price'],
            'course_dore_cat_id'=>$id,

        ]);
        $db->save();

        return redirect()->route('Course_dore_cat_sub_index' ,['id' => $id , 'name' => $name]);
    }



    public function Course_dore_cat_sub_active(Request $request){

        $id = $request->id;
        $db = Course_dore_cat_sub::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_sub',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Course_dore_cat_sub_delete(Request $request){
        $id = $request->id;
        $db = Course_dore_cat_sub::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_sub',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }




    public function Course_dore_cat_gallery($id , $name){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Course_dore_cat_gallery::where('course_dore_cat_id', $id)->where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.course_dore_cat_gallery.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Course_dore_cat_gallery_create($id , $name){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.course_dore_cat_gallery.create' , compact('id' , 'name'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Course_dore_cat_gallery_store(Request $request , $id , $name){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',


        ]);

        $db = Course_dore_cat_gallery::create([
            'name'=>$request['name'],
            'course_dore_cat_id'=>$id,

        ]);
        $db->save();

        return redirect()->route('Course_dore_cat_gallery_index' ,['id' => $id , 'name' => $name]);
    }



    public function Course_dore_cat_gallery_active(Request $request){

        $id = $request->id;
        $db = Course_dore_cat_gallery::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_gallery',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Course_dore_cat_gallery_delete(Request $request){
        $id = $request->id;
        $db = Course_dore_cat_gallery::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_gallery',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }

    public function Course_dore_cat_gallery_img(Request $request){
        $id = $request->id;
        $db = Course_dore_cat_gallery::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_gallery',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



    public function Course_dore_cat_question($id , $name){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Course_dore_cat_question::where('course_dore_cat_id', $id)->where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.course_dore_cat_question.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Course_dore_cat_question_create($id , $name){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.course_dore_cat_question.create' , compact('id' , 'name'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Course_dore_cat_question_store(Request $request , $id , $name){
        Controller::session();
        $this->validate($request,[
            'title' => 'required',
            'text' => 'required',


        ]);

        $db = Course_dore_cat_question::create([
            'title'=>$request['title'],
            'text'=>$request['text'],
            'course_dore_cat_id'=>$id,

        ]);
        $db->save();

        return redirect()->route('Course_dore_cat_question_index' ,['id' => $id , 'name' => $name]);
    }



    public function Course_dore_cat_question_active(Request $request){

        $id = $request->id;
        $db = Course_dore_cat_question::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_question',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Course_dore_cat_question_delete(Request $request){
        $id = $request->id;
        $db = Course_dore_cat_question::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_course_dore_cat_question',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



}