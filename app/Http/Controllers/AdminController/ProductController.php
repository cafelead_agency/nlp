<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:37 PM
 */

namespace App\Http\Controllers\AdminController;


use App\Http\Controllers\Controller;
use App\Model\Site\course_dore_cat;
use App\Model\Site\Course_dore_cat_gallery;
use App\Model\Site\Course_dore_cat_question;
use App\Model\Site\Course_dore_cat_sub;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function Product_index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Product::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Product.index', compact('db', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }

    public function Product_create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            $category = Product_cat::all();
            $tag = Product_tag::all();
            return view('admin_panel.Product.create' , compact('category' , 'tag'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }


    public function Product_store(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
            'text' => 'required',
            'price' => 'required',
            'category' => 'required',
        ]);


        if ($request->discount == null){
            $discount = 0;
        }else{
            $discount = $request->discount;
        }
        $db = Product::create([
            'name'=>$request['name'],
            'text'=>$request['text'],
            'price'=>$request['price'],
            'cat_id'=>$request['category'],
            'tag_id'=>$request['tag_id'],
            'discount'=>$discount,

        ]);
        $db->save();

        return redirect()->route('Product_index');
    }



    public function Product_edit($id){
        Controller::session();
        $edit = Controller::check_permission(3 );


        if($edit == 1){
            $category = Product_cat::all();
            $tag = Product_tag::all();
            $db = Product::where('id', $id)->first();
            return view('admin_panel.Product.edit' ,  compact('db' , 'category' , 'tag'));
        } else {

            return view('error.pages-403');
        }
    }


    public function Product_update(Request $request , $id)
    {
        $db = Product::where('id' , $id)->first();

        if ($request->has('name')) {
            $db->name = $request->get('name');
        }
        if ($request->has('text')) {
            $db->text = $request->get('text');
        }
        if ($request->has('price')) {
            $db->price = $request->get('price');
        }
        if ($request->has('category')) {
            $db->cat_id = $request->get('category');
        }
        if ($request->has('discount')) {
            $db->discount = $request->get('discount');
        }
        if ($request->has('tag_id')) {
            $db->tag_id = $request->get('tag_id');
        }

        $db->save();

        Controller::log_admin('tbl_product',$db->id,'update');

        return redirect()->route('Product_index');
    }




    public function Product_upload_img(Request $request){
        $id = $request->id;
        $db = Product::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_product',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_active(Request $request){

        $id = $request->id;
        $db = Product::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_product',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_delete(Request $request){
        $id = $request->id;
        $db = Product::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_product',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



    public function Product_cat_index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Product_cat::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Product_cat.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Product_cat_create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Product_cat.create' , compact('id' , 'name'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Product_cat_store(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $db = Product_cat::create([
            'name'=>$request['name'],

        ]);
        $db->save();

        return redirect()->route('Product_cat_index');
    }



    public function Product_cat_active(Request $request){

        $id = $request->id;
        $db = Product_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_product_cat',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_cat_delete(Request $request){
        $id = $request->id;
        $db = Product_cat::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_product_cat',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_tag_index(){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Product_tag::where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Product_tag.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Product_tag_create(){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Product_tag.create');
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Product_tag_store(Request $request){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $db = Product_tag::create([
            'name'=>$request['name'],

        ]);
        $db->save();

        return redirect()->route('Product_tag_index');
    }



    public function Product_tag_active(Request $request){

        $id = $request->id;
        $db = Product_tag::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_product_tag',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_tag_delete(Request $request){
        $id = $request->id;
        $db = Product_tag::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_product_tag',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }



    public function Product_gallery_index($id , $name){
        Controller::session();
        $show = Controller::check_permission(1 );
        $pic_permission = Controller::check_permission(5 );
        $delete_permission = Controller::check_permission(4 );
        $active_permission = Controller::check_permission(6 );
        $db = Product_gallery::where('product_id', $id)->where('is_delete', '=', 0)->latest()->paginate(10);

        if($show == 1) {
            return view('admin_panel.Product_gallery.index', compact('db','name' , 'id', 'pic_permission', 'delete_permission', 'active_permission'));
        } else {
            return view('admin_panel.pages.error.pages-403');
        }

    }


    public function Product_gallery_create($id , $name){
        Controller::session();
        $show = Controller::check_permission(2);
        if($show == 1){
            return view('admin_panel.Product_gallery.create' , compact('id' , 'name'));
        } else {

            return view('admin_panel.pages.error.pages-403');
        }
    }

    public function Product_gallery_store(Request $request , $id , $name){
        Controller::session();
        $this->validate($request,[
            'name' => 'required',


        ]);

        $db = Product_gallery::create([
            'name'=>$request['name'],
            'product_id'=>$id,

        ]);
        $db->save();

        return redirect()->route('Product_gallery_index' ,['id' => $id , 'name' => $name]);
    }



    public function Product_gallery_active(Request $request){

        $id = $request->id;
        $db = Product_gallery::where('id', $id)->first();
        if($db)
        {
            $db->is_active = $request->active;
            $db->save();
            Controller::log_admin('tbl_product_gallery',$id,"is_active = $request->active");
            $msg = 'فعال یا غیر فعالسازی با موفقیت انجام شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_active';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }


    public function Product_gallery_delete(Request $request){
        $id = $request->id;
        $db = Product_gallery::where('id', $id)->first();
        if($db)
        {
            $db->is_delete = 1;
            $db->save();
            Controller::log_admin('tbl_product_gallery',$id,"is_delete = 1");
            $msg = 'با موفقیت حذف شد';
            $status = 'success';
        }
        $data = array();
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'is_delete';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }

    public function Product_gallery_img(Request $request){
        $id = $request->id;
        $db = Product_gallery::where('id', $id)->first();
        if($db)
        {
            $db->img = $request->url;
            $db->save();
            Controller::log_admin('tbl_product_gallery',$id,'upload_img');
            $msg = 'تصویر شما با موفقیت تغییر کرد';
            $status = 'success';
        }
        $data = array();
        $data['img'] =  $db->img;
        $data['msg'] =  $msg;
        $data['status'] = $status;
        $myObj = array();
        $myObj['data'] = $data;
        $myObj['act'] = 'upload_img';
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }







}