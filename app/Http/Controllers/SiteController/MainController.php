<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 6/12/2019
 * Time: 6:51 PM
 */

namespace App\Http\Controllers\SiteController;


use App\Http\Controllers\Controller;
use App\Model\Site\AboutCompany;
use App\Model\Site\AboutPeople;
use App\Model\Site\Article;
use App\Model\Site\Article_cat;
use App\Model\Site\Contact_us_Form;
use App\Model\Site\course_dore_cat;
use App\Model\Site\course_one_day;
use App\Model\Site\CustomerAdvise;
use App\Model\Site\News;
use App\Model\Site\NLP_Result;
use App\Model\Site\Product;
use App\Model\Site\Product_cat;
use App\Model\Site\Product_gallery;
use App\Model\Site\Product_tag;
use App\Model\Site\Question;
use App\Model\Site\Video;
use App\Model\Site\Video_cat;
use Illuminate\Http\Request;

class MainController extends Controller
{
     public function index(){
         $db_dore_cat = Course_dore_cat::where('is_delete',0)->where('is_active' , 1)->get();
         $db_one_day = course_one_day::where('is_delete',0)->where('is_active' , 1)->get();
         $db_product_cat = Product_cat::where('is_delete',0)->where('is_active' , 1)->get();
         $db_article = Article::where('is_delete',0)->where('is_active' , 1)->take(3)->get();
         $db_Customer_Advise = CustomerAdvise::where('is_delete',0)->where('is_active' , 1)->get();
         $db_NLP_result = NLP_Result::where('is_delete',0)->where('is_active' , 1)->get();
         return view('Site.index' , compact('db_dore_cat' , 'db_one_day' , 'db_product_cat' , 'db_article' , 'db_Customer_Advise' , 'db_NLP_result'));
     }


    public function course_dore_cat_single($id){

        $db = Course_dore_cat::where('id',$id)->first();
        return view('Site.course_dore_cat_single' , compact('db'));
    }


    public function course_list(){

        $db_one_day = course_one_day::where('is_delete',0)->where('is_active' , 1)->get();

        $db_dore_cat = Course_dore_cat::where('is_delete',0)->where('is_active' , 1)->get();

        return view('Site.course_list' , compact('db_dore_cat' , 'db_one_day'));
    }



    public function course_one_day_single($id){

        $db = course_one_day::where('id',$id)->first();
        return view('Site.course_one_day_single' , compact('db'));
    }


    public function product_single($id){

        $db = Product::where('id',$id)->first();
        $gallery = Product_gallery::where('product_id' , $id)->where('is_delete' , 0)->where('is_active' , 1)->get();
        return view('Site.product_single' , compact('db','gallery'));
    }


    public function Product_list(){

        $tag = Product_tag::all();
        $cat = Product_cat::all();

        return view('Site.product_list' , compact('tag' , 'cat'));
    }


    public function News_list(){


        $db = News::where('is_delete',0)->where('is_active' , 1)->get();
        $db_important = News::where('is_delete',0)->where('is_active' , 1)->where('is_star' , 1)->take(4)->get();
        $db_new = News::where('is_delete',0)->where('is_active' , 1)->orderBy('id', 'DESC')->take(4)->get();
        $db_single = News::where('is_delete',0)->where('is_active' , 1)->take(1)->get();


        return view('Site.News_list' , compact('db' , 'db_important' , 'db_single' , 'db_new'));
    }


    public function News_single($id){

        $db = News::where('id',$id)->first();
        $db_new = News::where('is_delete',0)->where('is_active' , 1)->orderBy('id', 'DESC')->take(4)->get();

        return view('Site.News_single' , compact('db','db_new'));
    }

    public function About_us(){

         $people = AboutPeople::where('is_delete',0)->where('is_active' , 1)->get();
         $co = AboutCompany::where('id',1)->first();

        return view('Site.about' , compact('people' , 'co'));
    }


    public function Question_list(){
         $db = Question::where('is_delete',0)->get();
         return view('Site.Question' , compact('db'));
    }


    public function Contact_us(){
        return view('Site.Contact_us');
    }


    public function Contact_us_form(Request $request){

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'tel' => 'required|numeric',
            'text' => 'required',
        ]);

        $db = Contact_us_Form::create([
            'name'=>$request['name'],
            'email'=>$request['email'],
            'tel'=>$request['tel'],
            'text'=>$request['text'],
        ]);
        $db->save();

        return redirect()->route('Contact_us');

    }



    public function Video_list(){
        $db = Video::where('is_delete',0)->where('is_active' , 1)->latest()->paginate(20);
        $cat = Video_cat::where('is_delete' , 0)->get();

        return view('Site.Video_list' , compact('db' , 'cat'));
    }

    public function Video_cat_list_by_id($id){
        $db = Video::where('cat_id' , $id)->where('is_delete',0)->where('is_active' , 1)->latest()->paginate(20);
        $cat = Video_cat::where('is_delete' , 0)->get();

        return view('Site.Video_list_by_cat_sub_id' , compact('db' , 'cat'));
    }

    public function Video_list_by_cat_sub_id($id){
        $db = Video::where('cat_sub_id' , $id)->where('is_delete',0)->where('is_active' , 1)->latest()->paginate(20);
        $cat = Video_cat::where('is_delete' , 0)->get();

        return view('Site.Video_list_by_cat_sub_id' , compact('db' , 'cat'));
    }


    public function Video_single($id){
        $db = Video::where('id' , $id)->first();
        $cat = Video_cat::where('is_delete' , 0)->get();

        return view('Site.Video_single' , compact('db' , 'cat'));
    }



    public function Article_list(){
        $db = Article::where('is_delete',0)->where('is_active' , 1)->latest()->paginate(20);
        $cat = Article_cat::where('is_delete' , 0)->get();
        $important = Article::where('is_delete',0)->where('is_active' , 1)->where('imprtant' , 1)->latest()->get();

        return view('Site.Article_list' , compact('db' , 'cat' , 'important'));
    }

    public function Article_list_by_cat($cat_id){
        $db = Article::where('cat_id',$cat_id)->where('is_delete',0)->where('is_active' , 1)->latest()->paginate(20);
        $cat = Article_cat::where('is_delete' , 0)->get();

        return view('Site.Article_list_by_cat' , compact('db' , 'cat' , 'important'));
    }


    public function Article_single($id){
        $db = Article::where('id' , $id)->first();
        $cat = Article_cat::where('is_delete' , 0)->get();

        $count_view = $db->count_view;

        $db->count_view = $count_view + 1;
        $db->save();

        return view('Site.Article_single' , compact('db' , 'cat'));
        }






}