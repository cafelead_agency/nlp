<?php

namespace App\Model\admin_panel;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;

class AdminUser extends Authenticatable
{


    protected $guarded = [];

    public $timestamps = false;

    protected $table = 'tbl_admin_users';

    protected $primarykey = 'id';

    protected $connection = 'mysql';



    public function subpermissions()
    {
        return $this->belongsToMany(SubPermission::class,'sub_per_users');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'sub_per_users');
    }


}
