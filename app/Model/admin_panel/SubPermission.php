<?php

namespace App\Model\admin_panel;


use Illuminate\Database\Eloquent\Model;

class SubPermission extends Model
{

    protected $fillable = ['name' , 'description'];

    protected $table = 'sub_permissions';

    public function admin_users()
    {
        return $this->belongsToMany(AdminUser::class,'sub_per_users');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'sub_per_users');
    }
}
