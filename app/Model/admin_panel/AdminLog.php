<?php

namespace App\Model\admin_panel;


use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_admin_log';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
