<?php

namespace App\Model\admin_panel;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name' , 'description'];

    protected $table = 'permissions';



    public function subpermissions()
    {
        return $this->belongsToMany(SubPermission::class,'sub_per_users');
    }

    public function admin_users()
    {
        return $this->belongsToMany(AdminUser::class,'sub_per_users');
    }
}
