<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_Users';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
