<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_question';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
