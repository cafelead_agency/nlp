<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Site_Setting extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_site_setting';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
