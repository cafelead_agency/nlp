<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Product_gallery extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_product_gallery';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
