<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class CustomerAdvise extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_customer_advise';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
