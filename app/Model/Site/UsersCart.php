<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class UsersCart extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_Users_card';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';

    protected $casts = ['cart_detail' => 'array'];

}
