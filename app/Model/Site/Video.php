<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_video';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
