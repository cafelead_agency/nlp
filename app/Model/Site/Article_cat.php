<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Article_cat extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_article_cat';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
