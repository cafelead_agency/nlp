<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Contact_us_Form extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_contact_us_form';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
