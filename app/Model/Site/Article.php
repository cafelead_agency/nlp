<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_article';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
