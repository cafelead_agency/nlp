<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Course_dore_cat_sub extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_course_dore_cat_sub';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
