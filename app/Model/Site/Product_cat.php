<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Product_cat extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_product_cat';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
