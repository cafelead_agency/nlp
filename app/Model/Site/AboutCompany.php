<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class AboutCompany extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_about_company';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
