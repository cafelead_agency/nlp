<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Product_tag extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_product_tag';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
