<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Course_one_day_gallery extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_course_one_day_gallery';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
