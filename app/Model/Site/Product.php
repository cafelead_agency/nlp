<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_product';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
