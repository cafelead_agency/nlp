<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Course_dore_cat_question extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_course_dore_cat_question';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
