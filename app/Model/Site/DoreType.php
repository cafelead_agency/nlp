<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class DoreType extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_dore_type';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
