<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_news';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
