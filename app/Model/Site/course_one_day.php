<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class course_one_day extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_course_one_day';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
