<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class UserDoreRegister extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_user_dore_register';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
