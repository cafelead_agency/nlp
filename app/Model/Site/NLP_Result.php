<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class NLP_Result extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_nlp_result';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
