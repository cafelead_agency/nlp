<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class Video_cat extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_video_cat';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
