<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class AboutPeople extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_about_people';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
