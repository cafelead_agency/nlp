<?php

namespace App\Model\Site;


use Illuminate\Database\Eloquent\Model;

class course_dore_cat extends Model
{
    protected $guarded = [];

    protected $table = 'tbl_course_dore_cat';

    public $timestamps = false;

    protected $primarykey = 'id';

    protected $connection = 'mysql';



}
