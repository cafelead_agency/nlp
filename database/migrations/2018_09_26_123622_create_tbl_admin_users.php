<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_admin_users',function(Blueprint $table){
            $table->increments('id');
            $table->text('firstname');
            $table->text('lastname');
            $table->string('email')->unique();
            $table->string('tel')->unique();
            $table->text('password');
            $table->text('img')->nullable();
            $table->integer('count_login')->default(0);
            $table->timestamp('last_login')->useCurrent();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_admin_users');
    }
}
