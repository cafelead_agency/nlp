<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCourseOneDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_course_one_day', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('img')->nullable();
            $table->text('info')->nullable();
            $table->text('about')->nullable();
            $table->text('sweet')->nullable();
            $table->text('teacher')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });


        Schema::create('tbl_course_one_day_sub', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_dore_cat_id')->unsigned();
            $table->text('name')->nullable();
            $table->text('expire')->nullable();
            $table->integer('price')->default(0);
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_course_one_day_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_dore_cat_id')->unsigned();
            $table->text('name')->nullable();
            $table->text('img')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_course_one_day_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_dore_cat_id')->unsigned();
            $table->text('title')->nullable();
            $table->text('text')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_course_one_day');
        Schema::dropIfExists('tbl_course_one_day_sub');
        Schema::dropIfExists('tbl_course_one_day_gallery');
        Schema::dropIfExists('tbl_course_one_day_question');

    }
}
