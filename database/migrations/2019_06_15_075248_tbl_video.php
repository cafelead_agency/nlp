<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('aparat')->nullable();
            $table->text('video')->nullable();
            $table->text('img')->nullable();
            $table->text('text')->nullable();
            $table->integer('cat_id');
            $table->integer('cat_sub_id');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });


        Schema::create('tbl_video_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_video_cat_sub', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('cat_id');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_video');
        Schema::dropIfExists('tbl_video_cat');
        Schema::dropIfExists('tbl_video_cat_sub');

    }
}
