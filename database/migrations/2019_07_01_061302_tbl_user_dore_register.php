<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblUserDoreRegister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_dore_register', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dore_type')->unsigned();
            $table->text('firstname');
            $table->text('lastname');
            $table->text('tel');
            $table->text('email');
            $table->text('comment');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_dore_type', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user_dore_register');
        Schema::dropIfExists('tbl_dore_type');
    }
}
