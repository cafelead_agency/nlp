<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('sub_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('permission_id');
            $table->timestamps();
        });

        Schema::create('sub_per_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_permission_id')->unsigned();
            $table->foreign('sub_permission_id')->references('id')->on('sub_permissions')->onDelete('cascade');

            $table->integer('permission_id')->unsigned();
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');

            $table->integer('admin_user_id')->unsigned();
            $table->foreign('admin_user_id')->references('id')->on('tbl_admin_users')->onDelete('cascade');

//            $table->primary(['sub_permission_id' , 'permission_id' ,'admin_user_id'],'sub_per_user_id');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('sub_permissions');
        Schema::dropIfExists('sub_per_users');
    }
}

