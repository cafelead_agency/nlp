<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSiteSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_site_setting', function (Blueprint $table) {

            $table->increments('id');
            $table->text('logo');
            $table->text('primary_number');
            $table->text('address');
            $table->text('telegram');
            $table->text('instagram');
            $table->text('whatsapp');
            $table->text('phone');
            $table->text('primary_poster');

            $table->text('f_title_nlp1');
            $table->text('f_title_nlp2');
            $table->text('f_title_nlp3');
            $table->text('f_title_nlp4');
            $table->text('f_title_nlp5');
            $table->text('f_title_nlp6');
            $table->text('f_title_nlp7');
            $table->text('f_title_nlp8');

            $table->text('f_text_nlp1');
            $table->text('f_text_nlp2');
            $table->text('f_text_nlp3');
            $table->text('f_text_nlp4');
            $table->text('f_text_nlp5');
            $table->text('f_text_nlp6');
            $table->text('f_text_nlp7');
            $table->text('f_text_nlp8');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_site_setting');
    }
}
