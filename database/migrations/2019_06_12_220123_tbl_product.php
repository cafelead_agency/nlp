<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->string('name');
            $table->text('img')->nullable();
            $table->text('text')->nullable();
            $table->integer('discount')->default(0);
            $table->text('price')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });


        Schema::create('tbl_product_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });


        Schema::create('tbl_product_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_product_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->text('name')->nullable();
            $table->text('img')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product');
        Schema::dropIfExists('tbl_product_cat');
        Schema::dropIfExists('tbl_product_tag');
        Schema::dropIfExists('tbl_product_gallery');

    }
}
