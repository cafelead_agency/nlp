<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('list');
            $table->integer('count_view')->default(0);
            $table->integer('important')->default(0);
            $table->text('img')->nullable();
            $table->text('text')->nullable();
            $table->integer('cat_id');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });


        Schema::create('tbl_article_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_article_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('img')->nullable();
            $table->integer('article_id');
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_article');
        Schema::dropIfExists('tbl_article_cat');
        Schema::dropIfExists('tbl_article_comment');

    }
}
