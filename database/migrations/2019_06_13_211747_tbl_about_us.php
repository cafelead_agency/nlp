<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAboutUs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_about_people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('img')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });

        Schema::create('tbl_about_company', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('is_delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_about_people');
        Schema::dropIfExists('tbl_about_company');

    }
}
