<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAdminLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_admin_log',function(Blueprint $table){
            $table->increments('id');
            $table->text('table_name');
            $table->integer('row_id')->default(0);
            $table->text('action');
            $table->unsignedInteger('action_by')->default(0);
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('action_by')
                ->references('id')
                ->on('tbl_admin_users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_admin_log');
    }
}
