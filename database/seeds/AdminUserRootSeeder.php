<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUserRootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_admin_users')->insert([
            'id'=>'1',
            'firstname'=>'root',
            'lastname'=>'root',
            'email'=>'root',
            'tel'=>'root',
            'img'=>'/admin_panel/images/users/root_avatar.png',
            'password'=>Hash::make("toor")
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'1',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'2',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'3',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'4',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'5',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'6',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'7',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);
        DB::table('sub_per_users')->insert([
            'sub_permission_id'=>'8',
            'permission_id'=>'1',
            'admin_user_id'=>'1'
        ]);


    }
}
