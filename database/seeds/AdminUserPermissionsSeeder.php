<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUserPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'id'=>'1',
            'name'=>'مدیریت کاربران ادمین'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'1',
            'name'=>'مشاهده ی لیست',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'2',
            'name'=>'افزودن',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'3',
            'name'=>'ویرایش',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'4',
            'name'=>'حذف',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'5',
            'name'=>'تغییر تصویر',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'6',
            'name'=>'فعال و غیرفعال سازی',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'7',
            'name'=>'تغییر کلمه عبور',
            'permission_id'=>'1'
        ]);
        DB::table('sub_permissions')->insert([
            'id'=>'8',
            'name'=>'مدبربت دسترسی ها',
            'permission_id'=>'1'
        ]);
    }
}
