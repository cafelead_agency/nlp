@include('Site.inc.header')
<div id="notfound">
    <div class="notfoundicon"></div>
    <div class="notfoundtext">
        <h1>صفحه مورد نظر شما یافت نشد.</h1>
        <p>در برابر یک مانع باید با حفظ هدف، از خود انعطاف پذیری نشان داد تا به هدف مورد نظر رسید.</p>
        <div class="dialogauthor">دکتر کوروش معدلی</div>
    </div>
</div>
@include('Site.inc.footer')
