@extends('admin_panel.inc.master')

@section('preheader')
    <body id="body">
    <i class="clock-preloader"></i>
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection


@section('content')
<!— start wrapper Bar-->

<div class="wrapper">




    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="add-user">
                            <h4 class="mt-0 header-title"> مدیریت زیر دسته بندی ویدیو ها</h4>

                            <div class="search-add">
                                <form role="search" class="app-search">
                                    {{ csrf_field() }}
                                    <div class="form-group mb-0" style="position: relative">

                                        <input type="search" data-table="order-table"
                                               class="form-control light-table-filter" placeholder="جستجو...">


                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>

                                <a href="{{ route('Video_index') }}"><span class="btn btn-danger ">بازگشت</span></a>
                                <a href="{{ route('Video_create_cat_sub') }}"><span class="btn btn-primary ">افزودن</span></a>

                            </div>
                        </div>
                        <div id="tbl_content" class="table-rep-plugin">
                            <img  id="clock">

                            <div  class="table-responsive b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="display nowrap table order-table table-striped">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checked_input" id="checkAll" onclick="if_checked(this)"></th>
                                        <th>#</th>
                                        <th data-priority="2">نام</th>
                                        <th data-priority="2">دسته بندی</th>
                                        <th data-priority="7">تاریخ تولید</th>
                                        <th data-priority="7">عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php $row_count=0; ?>
                                    @foreach($db->chunk(10) as $chunk)
                                    @foreach($chunk as $dbs)
                                        @php $row_count++ @endphp
                                    <tr class="tbl-row" data-row_id="{{ $dbs->id }}">
                                        <td><input type="checkbox" id="chk{{$dbs->id}}" class="checked_input" name="type" data-row_id="{{ $dbs->id }}" value="{{ $dbs->id }}" onclick="del_selected(this)"></td>
                                        <td>{{ $row_count }}</td>
                                        @php($cat = \App\Model\Site\Video_cat::where('id' , $dbs->cat_id)->first())


                                        <td><a >{{ $dbs->name }}</a></td>
                                        <td><a >{{ $cat->name }}</a></td>
                                        <td id="date">{{  jdate($dbs->created_at)->format('%d %B، %Y') }}</td>
                                        <td class="noExport noPrint">

                                          @if($delete_permission == 1)
                                            <button class="delete btn btn-danger btn-group-sm fa fa-trash"
                                                    data-row_id="{{ $dbs->id }}" onclick="delete_row(this)" data-toggle="tooltip" title="حذف"></button>
                                          @else
                                                <button class="delete btn btn-danger btn-group-sm fa fa-trash"
                                                        data-row_id="{{ $dbs->id }}" onclick="delete_not(this)" data-toggle="tooltip" title="حذف"></button>

                                              @endif
                                        </td>


                                    </tr>
                                        @endforeach
                                        @endforeach




                                    </tbody>


                                </table>



                            </div>
                            <div id="editor"></div>
                        </div>


                        <!--pagination-->

                        <div id="pagination" style='text-align: center'>
                            <ul class="pagination justify-content-center">
                            {!! $db->render() !!}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</div>

<!— end wrapper —>

@endsection


@section('script')


    <script src="/admin_panel/export_excel/jquery.table2excel.js"></script>
    <script src="/admin_panel/export_pdf/export_pdf.js"></script>

<script>





    function delete_row(a) {

        swal({
            title: "آیا از حذف این آیتم مطمئن هستید؟",
            text: "شما دیگر قادر به بازگرداندن این آیتم نخواهید بود.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger m-l-10",
            confirmButtonText: "بله",
            cancelButtonText: "خیر"
        }).then(function () {
            swal("حذف شد", "عملیات با موفقیت انجام شد .", "success");
            var id = a.getAttribute('data-row_id');
            var form_data = {act : 'delete',id : id};
            $.ajax({
                url: '{{ route('Video_delete_cat_sub') }}', // point to server-side PHP script
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                data: form_data,
                type: 'GET',
                success: function (response) {
                    var obj = JSON.parse(response);
                    console.log(obj.data.status);
                    if (obj.data.status == 'success'){
                        window.location.replace('{{ route('Video_index_cat_sub') }}');
                    }
                },
                error: function (response) {

                }
            });
        })

    }
     //on delete not
    function delete_not(s){
        swal({
            title: "شما مجاز به حذف نیستید!",
            type: "warning",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "باشه",
        });
    }








</script>
    </body>
@endsection


