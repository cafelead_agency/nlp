
<div class="container-fluid">
    <!— Page-Title —>
    <div class="row">
        <div class="col-sm-2">
            <div class="page-title-box">
                <h4 class="page-title">داشبورد</h4>

            </div>
        </div>
        <div id="bellAndSearchHolder" class="col-xs-6 " style="position: absolute; left: 5%; top: 7px">
            <div class="menu-extras topbar-custom">

                <ul class="float-right list-unstyled mb-0">

                    <li class="notification-list" id="bell_menu">
                        <a
                                class="nav-link dropdown-toggle arrow-none waves-effect "
                                data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false"><i id="bell"
                                                                               class="ti-bell noti-icon"></i> <span
                                    class="badge badge-pill badge-danger noti-icon-badge">3</span>
                        </a>

                        <div id="sub_bell" class="sub_bell">

                            <div id="sub_sub_bell" class="sub_sub_bell">

                            </div>


                            <div class="bell_li">


                            </div>

                        </div>

                    </li>





                    <li id="profile_menu" class="dropdown notification-list">
                        <div id="sub_pro" class="dropdown notification-list nav-pro-img sub_profile">
                            <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user"
                               data-toggle="dropdown"
                               href="#" role="button" aria-haspopup="false" aria-expanded="false">
@if(session('admin_user.img') != null)
                                <img src="{{ session('admin_user.img') }}" alt="{{ session('admin_user.firstname'). session('admin_user.lastname')}}" class="rounded-circle"></a>
                            @else
                                <img src="/admin_panel/images/users/root_avatar.png" alt="{{ session('admin_user.firstname'). session('admin_user.lastname')}}" class="rounded-circle"></a>
                            @endif
                            <div id="sub_profile">


                                <ul class="profile_list">

                                    <li>
                                        {{ session('admin_user.firstname') .' '. session('admin_user.lastname') }} <i class="fas fa-user"></i>
                                    </li>


                                    <li>
                                        <a href="{{ route('admin_logout') }}">خروج</a><i class="fas fa-power-off"></i>
                                    </li>


                                </ul>

                            </div>

                        </div>
                    </li>



                    <li id="profile_menu" class="dropdown notification-list">
                        <div id="sub_pro" class="dropdown notification-list nav-pro-img sub_profile">
                            <a href="{{ route('site_index') }}" ><span class="btn btn-primary " style="margin-top: 12px">ورود به سایت اصلی</span></a>



                        </div>
                    </li>






                    <li class="menu-item">
                        <!— Mobile menu toggle-->
                        <a class="navbar-toggle nav-link" id="mobileToggle">
                            <div class="lines"><span></span> <span></span> <span></span></div>
                        </a>
                        <!— End mobile menu toggle-->
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
<!— MENU Start —>
<div id="navbar-custom" class="navbar-custom">
    <div class="container-fluid">
        <div id="navigation">
            <!— Navigation Menu-->
            <ul id="ulNavigation" class="navigation-menu">
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('admin_dashboard') }}"><i class="ti-dashboard"></i> <span>داشبورد</span></a></li>

                <li class="has-submenu"><a class="nav-sub-link home-icon" id="t2" name="tagLink" href="#"><i class="ti-user"></i>کاربران</a>
                    <ul class="submenu">
                        <li><a href="{{ route('admin_users_index') }}">مدیریت ادمین ها</a></li>
                    </ul>
                </li>


                <li class="has-submenu"><a class="nav-sub-link home-icon" id="t2" name="tagLink" href="#"><i class="ti-user"></i>دوره ها</a>
                    <ul class="submenu">
                        <li><a href="{{ route('Course_dore_cat_index') }}">مدیریت دوره های (دوره)</a></li>
                        <li><a href="{{ route('Course_one_day_index') }}">مدیریت دوره های (کارگاه یک روزه)</a></li>
                    </ul>
                </li>

                <li class="has-submenu"><a class="nav-sub-link home-icon" id="t2" name="tagLink" href="#"><i class="ti-user"></i>محصولات</a>
                    <ul class="submenu">
                        <li><a href="{{ route('Product_index') }}">لیست محصولات</a></li>
                        <li><a href="{{ route('Product_cat_index') }}">لیست دسته بندی محصولات</a></li>
                        <li><a href="{{ route('Product_tag_index') }}">لیست تگ های محصولات</a></li>
                    </ul>
                </li>

                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Video_index') }}"><i class="ti-dashboard"></i> <span>مدیریت ویدیو ها</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('News_index') }}"><i class="ti-dashboard"></i> <span>اخبار سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('About_People_index') }}"><i class="ti-dashboard"></i> <span>مدیریت درباره ما در سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Question_index') }}"><i class="ti-dashboard"></i> <span>مدیریت سوالات متداول در سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Site_setting_index') }}"><i class="ti-dashboard"></i> <span>مدیریت تنظیمات سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Contact_form') }}"><i class="ti-dashboard"></i> <span>فرم تماس با ما سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Article_index') }}"><i class="ti-dashboard"></i> <span>مقالات سایت</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('Customer_Advise_index') }}"><i class="ti-dashboard"></i> <span>نظرات مشتریان</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('NLP_Result_index') }}"><i class="ti-dashboard"></i> <span>دستاوردها</span></a></li>
                <li class="has-submenu"><a class="nav-sub-link home-icon " id="t1" href="{{ route('User_registration_dore') }}"><i class="ti-dashboard"></i> <span>پیش ثبت نام دوره</span></a></li>




            </ul>
            <!— End navigation menu —>
        </div>

        <p id="demo"></p>
        <!— end navigation —>
    </div>
    <!— end container-fluid —>
</div>
<!— end navbar-custom —>
