
<!DOCTYPE html>
<html>
<head>
    <?php


        if (!isset($var)){
            $var = 0;
        }

    ?>

    @if($var == 1)
        @include('admin_panel.inc.head2')
    @elseif($var == 2)
        @include('admin_panel.inc.head3')
    @else
        @include('admin_panel.inc.head')
     @endif


    @yield('extra')

</head>


@yield('preheader')

<header id="topnav">
    @yield('header')
</header>

@yield('content')

<!— Footer —>
<footer class="footer">
    @include('admin_panel.inc.footer')
</footer>
<!— End Footer —>

<!-- start footer script-->
@yield('begin_script')
@include('admin_panel.inc.footer_script')
@yield('script')

</html>
