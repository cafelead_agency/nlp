<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">

<title>پنل مدیریت پرتال</title>

<link rel="shortcut icon" href="/admin_panel/images/favicon.ico">

<link rel="stylesheet" href="/admin_panel/morris/morris.css">

<link href="/admin_panel/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<link href="/admin_panel/css/icons.css" rel="stylesheet" type="text/css">

<link href="/admin_panel/css/style.css" rel="stylesheet" type="text/css">
<link href="/admin_panel/css/print.css" rel="stylesheet" media="print" type="text/css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<link href="/admin_panel/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">

<script src="/admin_panel/js/jquery.min.js"></script>

<script src="/admin_panel/js/incapp.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript" src="/admin_panel/export_excel/jquery.table2excel.js"></script>

<script src="/admin_panel/js/bootstrap-select.min.js"></script>









