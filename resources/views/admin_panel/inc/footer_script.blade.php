<!— jQuery —>
<script src="/admin_panel/js/jquery.min.js"></script>
<script src="/admin_panel/js/jquery.multi-select.js"></script>


<script src="/admin_panel/js/bootstrap.bundle.min.js"></script>
<script src="/admin_panel/js/jquery.slimscroll.js"></script>
<script src="/admin_panel/js/waves.min.js"></script>
<script src="/admin_panel/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="/admin_panel/raphael/raphael-min.js"></script>
<script src="/admin_panel/sweet-alert2/sweetalert2.min.js"></script>

<!— App js —>
<script src="/admin_panel/js/app.js"></script>
<script src="/admin_panel/js/incapp.js"></script>
<!— javascript —>
<script>
    var f = function scrollFunc() {
        var eventHandler = function event() {
            if (window.scrollY > 200) {
                document.getElementById("navbar-custom").style.margin = "-70px";
                document.getElementById("bell").style.color = "black";
                document.getElementById("ulNavigation").style.textAlign = "right";
            }
            else {
                document.getElementById("navbar-custom").style.margin = "0";
                document.getElementById("bell").style.color = "white";
                document.getElementById("ulNavigation").style.textAlign = "center";
            }
        }
        window.addEventListener('scroll', eventHandler, false);
    }
    document.addEventListener('DOMContentLoaded', f, false);



</script>
