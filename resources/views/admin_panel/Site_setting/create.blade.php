
<?php $var = 2; ?>

@extends('admin_panel.inc.master')

@section('extra')
    <style>
        a{
            color: #707070 !important;
        }
        span.badge{
            background-color: #dc3545 !important;
        }
        span.caret{
            display: none;
        }
        li{
            text-align: right;
        }
        span.pull-left{
            text-align: center !important;
        }
    </style>
    @endsection
@section('preheader')
    <body id="body">
    @endsection

@section('header')
    @include('admin_panel.inc.header')
@endsection

@section('content')
    <!— start wrapper Bar-->

    <div class="wrapper">
        <div class="container">


            <div class="row">
                <div id="form-holder">
                    <div class="card m-b-20">
                        <div class="card-body">

                            <h5>ویرایش تنظیمات سایت</h5>
                            <hr>
                            @include('admin_panel.pages.error.errors_temp')

                            <form action="{{ route('Site_setting_update') }}" method="post">
                              {{csrf_field()}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">شماره اصلی</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="primary_number" value="{{ $db->primary_number }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">آدرس</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="address" value="{{ $db->address }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">آدرس تلگرام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="telegram" value="{{ $db->telegram }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">آدرس اینستاگرام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="instagram" value="{{ $db->instagram }}">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">آدرس واتساپ</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="whatsapp" value="{{ $db->whatsapp }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">تلفن ها</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="phone" value="{{ $db->phone }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp1" value="{{ $db->f_title_nlp1 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp1" value="{{ $db->f_text_nlp1 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp2" value="{{ $db->f_title_nlp2 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp2" value="{{ $db->f_text_nlp2 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp3" value="{{ $db->f_title_nlp3 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp3" value="{{ $db->f_text_nlp3 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp4" value="{{ $db->f_title_nlp4 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp4" value="{{ $db->f_text_nlp4 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp5" value="{{ $db->f_title_nlp5 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp5" value="{{ $db->f_text_nlp5 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp6" value="{{ $db->f_title_nlp6 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp6" value="{{ $db->f_text_nlp6 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp7" value="{{ $db->f_title_nlp7 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp7" value="{{ $db->f_text_nlp7 }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_title_nlp8" value="{{ $db->f_title_nlp8 }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">کاربرد NLP</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="f_text_nlp8" value="{{ $db->f_text_nlp8 }}">
                                    </div>
                                </div>













                                <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ارسال</button>



                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!— end wrapper —>

@endsection

    @section('script')
        <script src="/admin_panel/ckeditor/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('text_content1');



        </script>
    </body>
@endsection
