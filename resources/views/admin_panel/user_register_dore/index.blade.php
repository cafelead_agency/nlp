@extends('admin_panel.inc.master')

@section('preheader')
    <body id="body">
    <i class="clock-preloader"></i>
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection


@section('content')
<!— start wrapper Bar-->

<div class="wrapper">




    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="add-user">
                            <h4 class="mt-0 header-title">لیست کاربران پیش ثبت نام در دوره</h4>
                            <div class="group_opreation">

                            </div>
                            <div class="search-add">
                                <form role="search" class="app-search">
                                    {{ csrf_field() }}
                                    <div class="form-group mb-0" style="position: relative">

                                        <input type="search" data-table="order-table"
                                               class="form-control light-table-filter" placeholder="جستجو...">


                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div id="tbl_content" class="table-rep-plugin">
                            <img  id="clock">

                            <div  class="table-responsive b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="display nowrap table order-table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th data-priority="2">نام</th>
                                        <th data-priority="2">نام خانوادگی</th>
                                        <th data-priority="2">شماره تلفن</th>
                                        <th data-priority="2">ایمیل</th>
                                        <th data-priority="2">توضیحات</th>
                                        <th data-priority="2">نوع دوره</th>
                                        <th data-priority="7">تاریخ ثبت نام</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php $row_count=0; ?>
                                    @foreach($db->chunk(10) as $chunk)
                                    @foreach($chunk as $dbs)
                                        @php $row_count++ @endphp
                                        @php $dore_type = \App\Model\Site\DoreType::where('id' , $dbs->id)->first(); @endphp

                                    <tr class="tbl-row" data-row_id="{{ $dbs->id }}">
                                        <td>{{ $row_count }}</td>

                                        <td><a >{{ $dbs->firstname }}</a></td>
                                        <td><a >{{ $dbs->lastname }}</a></td>
                                        <td><a >{{ $dbs->tel }}</a></td>
                                        <td><a >{{ $dbs->email }}</a></td>
                                        <td><a >{{ $dbs->comment }}</a></td>
                                        <td><a >{{ $dore_type->name }}</a></td>

                                        <td id="date">{{  jdate($dbs->created_at)->format('%d %B، %Y') }}</td>






                                    </tr>
                                        @endforeach
                                        @endforeach




                                    </tbody>


                                </table>



                            </div>
                            <div id="editor"></div>
                        </div>


                        <!--pagination-->

                        <div id="pagination" style='text-align: center'>
                            <ul class="pagination justify-content-center">
                            {!! $db->render() !!}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</div>

<!— end wrapper —>

@endsection


@section('script')


    <script src="/admin_panel/export_excel/jquery.table2excel.js"></script>
    <script src="/admin_panel/export_pdf/export_pdf.js"></script>

<script>

</script>
    </body>
@endsection


