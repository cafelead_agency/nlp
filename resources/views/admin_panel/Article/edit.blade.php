
<?php $var = 2; ?>

@extends('admin_panel.inc.master')

@section('extra')

    <style>
        a{
            color: #707070 !important;
        }
        span.badge{
            background-color: #dc3545 !important;
        }
        span.caret{
            display: none;
        }
        li{
            text-align: right;
        }
        span.pull-left{
            text-align: center !important;
        }
    </style>
@endsection
@section('preheader')
    <body id="body">
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection

@section('content')
    <!— start wrapper Bar-->

    <div class="wrapper">
        <div class="container">

            <div class="row">
                <div id="form-holder">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <a href="{{ route('Article_index') }}" class="go_bac_page"><span class="btn btn-blue" style="float: left;!important;">بازگشت</span></a>

                            <h5>ویرایش</h5>
                            <hr>

                            <form action="{{ route('Article_update' , ['id' => $db->id]) }}" method="post">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">نام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="name" value="{{ $db->name }}">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">فهرست</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="list" value="{{ $db->list }}">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">دسته بندی</label>
                                    <div class="col-sm-10">
                                        <select name="cat_id">
                                            @foreach($cat as $cats)
                                                <option value="{{ $cats->id }}">{{ $cats->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">متن خبر</label>
                                    <div class="col-sm-10">
                                        <textarea name="text" id="text_content1">{{ $db->text }}</textarea>

                                    </div>
                                </div>




                                <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ارسال</button>






                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!— end wrapper —>
@endsection

@section('script')

    </body>

    <script src="/admin_panel/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text_content1');



    </script>
@endsection


