<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>403 Error</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="/admin_panel/images/favicon.ico">
    <link href="/admin_panel/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin_panel/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/admin_panel/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Begin page -->
<div class="wrapper-page">
    <div class="card">
        <div class="card-block">
            <div class="ex-page-content text-center">
                <h1 class="text-dark">403<img src="/admin_panel/images/error-icon/6.png"></h1>
                <h4 class="font_iransans">درخواست شما توسط سرور منع شده است</h4>
                <br><a class="btn btn-info mb-5 waves-effect waves-light" href="{{ route('admin_dashboard') }}"><i class="mdi mdi-home"></i>بازگشت</a></div>
        </div>
    </div>

</div>
<!-- jQuery  -->
<script src="/admin_panel/js/jquery.min.js"></script>
<script src="/admin_panel/js/bootstrap.bundle.min.js"></script>
<script src="/admin_panel/js/jquery.slimscroll.js"></script>
<script src="/admin_panel/js/waves.min.js"></script>
<script src="/admin_panel/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- App js -->
<script src="/admin_panel/js/app.js"></script>
</body>

</html>
