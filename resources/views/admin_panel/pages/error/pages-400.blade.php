<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Lexa - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="/images/favicon.ico">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Begin page -->
<div class="wrapper-page">
    <div class="card">
        <div class="card-block">
            <div class="ex-page-content text-center">
                <h1 class="text-dark">400<img src="/images/error-icon/6.png"></h1>
                <h4 class="font_iransans">درخواست شما نامعتبر است</h4>
                <br><a class="btn btn-info mb-5 waves-effect waves-light" href="../admin_user/index.blade.php"><i class="mdi mdi-home"></i>بازگشت</a></div>
        </div>
    </div>
    <div class="m-t-40 text-center">
        <p>© 2018 KarsanIT پدیدآورنده تیم کارسان</p>
    </div>
</div>
<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/waves.min.js"></script>
<script src="/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- App js -->
<script src="/js/app.js"></script>
</body>

</html>
