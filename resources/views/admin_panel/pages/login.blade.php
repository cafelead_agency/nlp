
@extends('admin_panel.inc.master')


@section('preheader')
    <body>
    @endsection

@section('content')




<div class="wrapper-page">
    <div class="card" style="height: 500px;">
        <div class="card-body">

            <div class="p-3">
                <h4 class="text-muted font-18 m-b-5 text-center">خوش آمدید </h4>
                <p class="text-muted text-center">برای ادامه دادن وارد شوید.</p>


                @if($errors->any())
                    <h4 style="color: indianred;font-size: 28px;">{{$errors->first()}}</h4>
                @endif
                <div class="form-horizontal m-t-30" >
                    <form method="post" action="{{ route('admin_users_login_post') }}">
                        {{ csrf_field() }}
                    <div class="form-group">
                        <label for="username" style="float: right">نام کاربری</label>
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="نام کاربری را اینجا وارد کنید">
                    </div>
                    <div class="form-group">
                       <label for="userpassword" style="float: right">رمز عبور</label>
                        <input type="password" class="form-control" id="userpassword" name="password"
                               placeholder="رمز عبور را اینجا وارد کنید">
                    </div>
                   <div class="form-group row m-t-20">

                        <div class="col-6 text-right">
                            <button id="submit_login" class="btn btn-primary w-md waves-effect waves-light"
                                    type="submit">ورود
                            </button>
                        </div>
                    </div>
                    </form>
                    <div class="form-group m-t-10 mb-0 row">

                        <div class="col-12 m-t-20"><a href="#" class="text-muted">رمز خود را فراموش
                            کردید<i class="mdi mdi-lock"></i></a></div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
    @endsection

@section('script')
</body>

@endsection
