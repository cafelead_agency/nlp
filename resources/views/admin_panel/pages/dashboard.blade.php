@extends('admin_panel.inc.master')
@section('extra')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css">
    <style>
        .virayesh-rezom{
            padding: 1px 20px;
            margin-left: 10px;
            background: #ddd;
        }
        .virayesh-rezom:hover{
            background: #f3d474;
            transition: 1s all;
        }
        #imd-up-edi{
            width: 30%;
            margin-bottom: 20px;
        }
        .fc-left .fc-button-group{
            direction: rtl;
        }
        .fc-highlight{

        }
        .div-content2 {
            margin-left: 20px;
            margin-right: 10px;
            padding-right: 15px;
            padding-left: 15px;
            background: white;
            height: 613px;
            border-radius: 10px;
        }
        .fc-content{
            color: #fff;
        }
        .content-tt{
            text-align: center;
            padding: 0;
            background: #ebebe9;
            margin-top: 0;
            height: 550px;
            overflow-y: auto;
        }
        .content-ttt{
            text-align: center;
            padding: 0;
            background: #ebebe9;
            margin-top: 0;
            height: 499px;
            overflow-y: auto;
        }
        .tiket-e {
            background: #ddd;
            padding: 10px;
            line-height: 25px;
            border-radius: 4px;
        }

        .kadr-tik {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .tiket-d {
            background: #afc6ff;
            padding: 10px;
            line-height: 25px;
            border-radius: 4px;
        }

        .tiket-e::after {
            position: absolute;
            top: 27px;
            right: -14px;
            display: inline-block;
            border-top: 14px solid transparent;
            border-left: 14px solid #ddd;
            border-right: 0 solid #ddd;
            border-bottom: 14px solid transparent;
            content: " ";
        }

        .tiket-d::before {
            position: absolute;
            top: 27px;
            left: -14px;
            display: inline-block;
            border-top: 14px solid transparent;
            border-right: 14px solid #afc6ff;
            border-left: 0 solid #afc6ff;
            border-bottom: 14px solid transparent;
            content: " ";
        }

        .tiket-img {
            display: block;
            padding-top: 20px;
            margin: 0 auto;
        }
    </style>
@endsection
@section('preheader')
    <body id="body">
    @endsection
    @section('header')
        @include('admin_panel.inc.header')
    @endsection
    @section('content')
        <style>

            .dashboard_cart div.cart_hold {

                height: 200px;
                border: 1px solid;
                border-radius: 4px;
                background-image: linear-gradient(to right top, #d16ba5, #c777b9, #ba83ca, #aa8fd8, #9a9ae1, #8aa7ec, #79b3f4, #69bff8, #52cffe, #41dfff, #46eefa, #5ffbf1);
                color: #fff;
                transition: transform .2s;
            }
            .dashboard_cart div.cart_hold:hover{
                transform: scale(1.2);
            }

            .twins {
                height: 100%;
                width: 50%;
            }
            .twins ul{
                right: 29px;
                position: absolute;
                text-align: right;
            }

            .cart_icon {
                font-size: 30px;
                width: 64px;
                height: 64px;
                line-height: 64px;
                text-align: center;
                color: #fff !important;
                border-radius: 50%;
                background: rgba(255, 255, 255, .1);
                display: inline-block;
                float: left;
                margin: 10px 30px;
            }

            div.container {
                padding-top: 200px;
            }
        </style>
        <div class="container">
            <div class="row">
                {{--<div class="col-md-4 dashboard_cart">--}}
                    {{--<a href="{{ route('index') }}">--}}
                        {{--<div class="cart_hold">--}}
                            {{--<div class="row">--}}
                                {{--<div class="twins">--}}
                                    {{--<span class="fa fa-user cart_icon"></span>--}}
                                {{--</div>--}}
                                {{--<div class="twins">--}}
                                    {{--<ul>--}}
                                        {{--<li><h4>کاربران ادمین</h4></li>--}}
                                        {{--<li><h3>{{ $count }}</h3></li>--}}
                                        {{--<li><span class="badge badge-info">{{ $count }}</span>--}}
                                        {{--<span class="ml-2">استخدام شده</span>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}


                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}

                {{--</div>--}}

            </div>
        </div>
        <!— end row —>
        <!— end container-fluid —>


        <!— end wrapper —>
    @endsection
    @section('script')
    </body>
@endsection
