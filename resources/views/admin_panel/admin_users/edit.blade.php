
<?php $var = 2; ?>

@extends('admin_panel.inc.master')

@section('extra')

    <style>
        a{
            color: #707070 !important;
        }
        span.badge{
            background-color: #dc3545 !important;
        }
        span.caret{
            display: none;
        }
        li{
            text-align: right;
        }
        span.pull-left{
            text-align: center !important;
        }
    </style>
@endsection
@section('preheader')
    <body id="body">
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection

@section('content')
    <!— start wrapper Bar-->

    <div class="wrapper">
        <div class="container">

            <div class="row">
                <div id="form-holder">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <a href="{{ route('admin_users_index') }}" class="go_bac_page"><span class="btn btn-blue" style="float: left;!important;">بازگشت</span></a>

                            <h5>ویرایش</h5>
                            <hr>

                            <form action="{{ route('admin_users_update' , ['id' => $db->id]) }}" method="post">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">نام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="first_name" value="{{ $db->firstname }} " id="first_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">نام خانوادگی</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" value="{{ $db->lastname }}" id="last_name" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">شماره تلفن</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" value="{{ $db->tel }} " id="phone" name="phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ایمیل</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" value="{{ $db->email }} " id="email" name="email">
                                    </div>
                                </div>




                                <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ارسال</button>






                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!— end wrapper —>
@endsection

@section('script')

    </body>
@endsection


