@extends('admin_panel.inc.master')

@section('preheader')
    <body id="body">
    <i class="clock-preloader"></i>
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection


@section('content')
<!— start wrapper Bar-->

<div class="wrapper">

    <div class="toolsbar noPrint">
        <div class="toolsbarin text-center">
            <img onclick="printData()" id="peint_table" src="/admin_panel/images/icon/5.png" height="20%"><br>
            <img id="export_excel" src="/admin_panel/images/icon/008-excel.png" height="20%"><br>
            <img onclick="downloadPDF()" src="/admin_panel/images/icon/4.png" height="20%">
            <div class="toolsbarbox"><img src="/admin_panel/images/icon/send.png" class="toolsbarboximg"></div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="add-user">
                            <h4 class="mt-0 header-title"> مدیریت کاربران ادمین</h4>
                            <div class="group_opreation">
                                <form action="#" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" id="ids_checkbox" name="ids_checkbox">
                                    <button class="btn btn-danger fa fa-trash" type="submit" name="group_act" value="delete"></button>
                                    <button class="btn btn-success btn-sm "  type="submit" name="group_act" value="active"> فعال</button>
                                    <button class="btn btn-dark btn-sm "  type="submit" name="group_act" value="deactive">غیر فعال</button>
                                </form>
                            </div>
                            <div class="search-add">
                                <form role="search" class="app-search">
                                    {{ csrf_field() }}
                                    <div class="form-group mb-0" style="position: relative">

                                        <input type="search" data-table="order-table"
                                               class="form-control light-table-filter" placeholder="جستجو...">


                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>

                                <a href="{{ route('admin_users_create') }}"><span class="btn btn-primary ">افزودن</span></a>

                            </div>
                        </div>
                        <div id="tbl_content" class="table-rep-plugin">
                            <img  id="clock">

                            <div  class="table-responsive b-0" data-pattern="priority-columns">
                                <table id="tech-companies-1" class="display nowrap table order-table table-striped">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checked_input" id="checkAll" onclick="if_checked(this)"></th>
                                        <th>#</th>
                                        <th data-priority="1">عکس</th>
                                        <th data-priority="2">نام</th>
                                        <th data-priority="3">نام خانوادگی</th>
                                        <th data-priority="3">ایمیل</th>
                                        <th data-priority="4">شماره تماس</th>
                                        <th data-priority="7">تاریخ عضویت</th>
                                        <th data-priority="7">عملیات</th>
                                        <th data-priority="7">فعال/غیر فعال</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php $row_count=0; ?>
                                    @foreach($admin_users->chunk(10) as $chunk)
                                    @foreach($chunk as $admin_user)
                                        @php $row_count++ @endphp
                                    <tr class="tbl-row" data-row_id="{{ $admin_user->id }}">
                                        <td><input type="checkbox" id="chk{{$admin_user->id}}" class="checked_input" name="type" data-row_id="{{ $admin_user->id }}" value="{{ $admin_user->id }}" onclick="del_selected(this)"></td>
                                        <td>{{ $row_count }}</td>
                                        <td>
                                          @if($pic_permission == 1)
                                            @if($admin_user->img == null)
                                            <div id="image" onclick="Kcfinder(this, '{{route('admin_user_upload_img')}}' ,'img')" data-row_id="{{ $admin_user->id }}">
                                                <img id="{{'img-'.$admin_user->id}}" class="userImg" src="/admin_panel/images/users/nophoto.png" >
                                                <div class="overlay-photo">
                                                   <div class="text-photo">تغییر دادن</div>
                                                </div>
                                            </div>
                                            @elseif($admin_user->img != null)
                                                <div id="image" onclick="Kcfinder(this, '{{route('admin_user_upload_img')}}' ,'img')" data-row_id="{{ $admin_user->id }}">
                                                    <img id="{{'img-'.$admin_user->id}}" class="userImg" src="{{ $admin_user->img }}">
                                                    <div class="overlay-photo">
                                                        <div class="text-photo">تغییر دادن</div>
                                                    </div>
                                                </div>
                                            @endif
                                            @else
                                                @if($admin_user->img == null)
                                                    <div id="image" data-row_id="{{ $admin_user->id }}">
                                                        <img id="img" class="userImg" src="/admin_panel/images/users/nophoto.png" >
                                                        <div class="overlay-photo">
                                                            <div class="text-photo">نداشتن مجوز</div>
                                                        </div>
                                                    </div>
                                                @elseif($admin_user->img != null)
                                                    <div id="image" data-row_id="{{ $admin_user->id }}">
                                                        <img id="img" class="userImg" src="{{ $admin_user->img }}">
                                                        <div class="overlay-photo">
                                                            <div class="text-photo">نداشتن مجوز</div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif



                                        </td>
                                        <td><a >{{ $admin_user->firstname }}</a></td>
                                        <td >{{ $admin_user->lastname }}</td>
                                        <td >{{ $admin_user->email }}</td>
                                        <td >{{ $admin_user->tel }}</td>

                                        <td id="date">{{  jdate($admin_user->created_at)->format('%d %B، %Y') }}</td>
                                        <td class="noExport noPrint">

                                            <a href="{{ route('admin_users_edit' ,['id' => $admin_user->id]) }}"><span class="btn btn-success btn-group-sm fa fa-edit" data-toggle="tooltip" title="ویرایش مشخصات"></span></a>
                                            <a href="{{ route('permission_list' , ['id' => $admin_user->id]) }}"><span class="btn btn-lime btn-group-sm fas fa-level-up-alt" data-toggle="tooltip" title="سِمت های کاربر"></span></a>
                                          @if($delete_permission == 1)
                                            <button class="delete btn btn-danger btn-group-sm fa fa-trash"
                                                    data-row_id="{{ $admin_user->id }}" onclick="delete_row(this)" data-toggle="tooltip" title="حذف کاربر"></button>
                                          @else
                                                <button class="delete btn btn-danger btn-group-sm fa fa-trash"
                                                        data-row_id="{{ $admin_user->id }}" onclick="delete_not(this)" data-toggle="tooltip" title="حذف کاربر"></button>

                                              @endif

                                            @if($changepassword_permission == 1)
                                            <a href="#" class="change_password" data-row_id="{{ $admin_user->id }}"
                                               onclick="change_password(this)"> <span class="btn btn-block btn-dark">تغییر کلمه عبور</span></a>
                                                @else
                                                <a href="#" class="change_password" data-row_id="{{ $admin_user->id }}"
                                                   onclick="change_password_not(this)"> <span class="btn btn-block btn-dark">تغییر کلمه عبور</span></a>
                                            @endif
                                        </td>

                                          @if($admin_user->is_active == 1)
                                              <?php $check = "checked='checked'"?>
                                              @else
                                              <?php $check = ""?>
                                        @endif

                                        <td>
                                            @if($active_permission == 1)
                                        <input type="checkbox" id="{{"switch-".$admin_user->id}}" switch="success" {{$check}}
                                                   data-row_id="{{ $admin_user->id }}" onclick="user_active(this)">
                                            <label for="{{"switch-".$admin_user->id}}" data-on-label="فعال" data-off-label="غیر فعال"></label>
                                                @else
                                                <input type="checkbox" id="{{"switch-".$admin_user->id}}" switch="success" {{$check}}
                                                data-row_id="{{ $admin_user->id }}" onclick="user_active_not(this)">
                                                <label data-on-label="فعال" data-off-label="غیر فعال" data-toggle="tooltip" title="نداشتن مجوز"></label>
                                            @endif
                                        </td>

                                    </tr>
                                        @endforeach
                                        @endforeach




                                    </tbody>


                                </table>



                            </div>
                            <div id="editor"></div>
                        </div>


                        <!--pagination-->

                        <div id="pagination" style='text-align: center'>
                            <ul class="pagination justify-content-center">
                            {!! $admin_users->render() !!}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</div>

<!— end wrapper —>

@endsection


@section('script')


    <script src="/admin_panel/export_excel/jquery.table2excel.js"></script>
    <script src="/admin_panel/export_pdf/export_pdf.js"></script>

<script>





    /*change password*/
    function change_password(a) {

        swal({
            title: "لطفا کلمه عبور جدید را وارد کنید",
            input: "password",
            showCancelButton: !0,
            confirmButtonText: "ارسال",
            cancelButtonText: "لغو",
            showLoaderOnConfirm: !0,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger m-l-10",
            preConfirm: function (n) {
                var m = n.length;
                return new Promise(function (t, e) {
                    setTimeout(function () {
                        if (n === "123") {
                            e("پر کردن فیلد پسورد الزامی است.");
                        } else if (m < 5) {
                            e("پسورد باید بیشتر از 5 کاراکتر باشد");
                        } else {
                            t();
                        }
                    }, 2e3)
                })
            },
            allowOutsideClick: !1
        }).then(function (t) {
            var id = a.getAttribute('data-row_id');
            var form_data = {act: 'user_admin_change_password', id: id, pass_value: t};
            $.ajax({
                type: 'GET',
                url: '{{ route('admin_user_change_password') }}', // point to server-side PHP script
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                // contentType: false,
                data: form_data,
                success: function (response) {
                    swal({
                        type: "success",
                        title: "کلمه عبور با موفقیت تغییر یافت",
                        html: "کلمه عبور جدید : " + t
                    })
                },

                error: function (response) {
                    swal({
                        type: "error",
                        title: "پاسخی از سرور دریافت نشد، بعدا دوباره امتحان کنید",

                    })
                }

            });
        })


    }
//on changepassword not

    function  change_password_not(s) {

        swal({
            title: "شما مجاز به تفییر کلمه عبور نیستید!",
            type: "warning",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "باشه",
        });
    }

    <!-- active and deactive user -->
    function user_active(input) {


        var is_active = 0;
        var idAttr = input.getAttribute('id');
        var checkBox = document.getElementById(idAttr);
        if (checkBox.checked == true) {
            is_active = 1;

        } else {
            is_active = 0;
        }



        var id = input.getAttribute('data-row_id');
        var form_data = {act : 'is_active',id : id , active : is_active};
        $.ajax({
            url: '{{ route('admin_user_is_active') }}', // point to server-side PHP script
            dataType: 'text', // what to expect back from the PHP script
            cache: false,
            data: form_data,
            type: 'GET',
            success: function (response) {

            },
            error: function (response) {

            }


        });
    }
    function  user_active_not(s) {

        swal({

            title: "شما مجاز به تغییر وضعیت نیستید!",
            type: "warning",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "باشه",
        });
    }

    function delete_row(a) {

        swal({
            title: "آیا از حذف این آیتم مطمئن هستید؟",
            text: "شما دیگر قادر به بازگرداندن این آیتم نخواهید بود.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger m-l-10",
            confirmButtonText: "بله",
            cancelButtonText: "خیر"
        }).then(function () {
            swal("حذف شد", "عملیات با موفقیت انجام شد .", "success");
            var id = a.getAttribute('data-row_id');
            var form_data = {act : 'delete',id : id};
            $.ajax({
                url: '{{ route('admin_user_is_delete') }}', // point to server-side PHP script
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                data: form_data,
                type: 'GET',
                success: function (response) {
                    var obj = JSON.parse(response);
                    console.log(obj.data.status);
                    if (obj.data.status == 'success'){
                        window.location.replace('{{ route('admin_users_index') }}');
                    }
                },
                error: function (response) {

                }
            });
        })

    }
     //on delete not
    function delete_not(s){
        swal({
            title: "شما مجاز به حذف نیستید!",
            type: "warning",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "باشه",
        });
    }








</script>
    </body>
@endsection


