
<?php $var = 2; ?>

@extends('admin_panel.inc.master')

@section('extra')

    <style>
        a{
            color: #707070 !important;
        }
        span.badge{
            background-color: #dc3545 !important;
        }
        span.caret{
            display: none;
        }
        li{
            text-align: right;
        }
        span.pull-left{
            text-align: center !important;
        }
    </style>
@endsection
@section('preheader')
    <body id="body">
@endsection


@section('header')
    @include('admin_panel.inc.header')
@endsection

@section('content')
    <!— start wrapper Bar-->

    <div class="wrapper">
        <div class="container">

            <div class="row">
                <div id="form-holder">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <a href="{{ route('Course_dore_cat_index') }}" class="go_bac_page"><span class="btn btn-blue" style="float: left;!important;">بازگشت</span></a>

                            <h5>ویرایش</h5>
                            <hr>

                            <form action="{{ route('Course_dore_cat_update' , ['id' => $db->id]) }}" method="post">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">نام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="name" value="{{ $db->name }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">چیست</label>
                                    <div class="col-sm-10">
                                        <textarea name="info" id="text_content1">{{ $db->info }}</textarea>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">مطالب دوره</label>
                                    <div class="col-sm-10">
                                        <textarea name="about" id="text_content2">{{ $db->about }}</textarea>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">مناسب چه کسانی است</label>
                                    <div class="col-sm-10">
                                        <textarea name="sweet" id="text_content3">{{ $db->sweet }}</textarea>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">درباره مدرس</label>
                                    <div class="col-sm-10">
                                        <textarea name="teacher" id="text_content4">{{ $db->teacher }}</textarea>

                                    </div>
                                </div>




                                <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ارسال</button>






                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!— end wrapper —>
@endsection

@section('script')

    </body>

    <script src="/admin_panel/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text_content1');
        CKEDITOR.replace('text_content2');
        CKEDITOR.replace('text_content3');
        CKEDITOR.replace('text_content4');


    </script>
@endsection


