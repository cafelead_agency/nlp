
<?php $var = 1; ?>
@extends('admin_panel.inc.master')


@section('extra')


<style>
    a{
        color: #707070 !important;
    }
    span.badge{
        background-color: #dc3545 !important;
    }
    span.caret{
        display: none;
    }
    li{
        text-align: right;
    }
    span.pull-left{
        text-align: center !important;
    }


</style>




@endsection
@section('preheader')
    <body id="body" onload="loadpage()">
    @endsection


<!— Navigation Bar-->


<!— End Navigation Bar-->
@section('header')
    @include('admin_panel.inc.header')
@endsection
<!— start wrapper Bar-->
@section('content')
<div class="wrapper">
    <div class="container">

        <div class="row">
            <div id="form-holder">
                <div class="card m-b-20">
                    <div class="card-body">
                        <a href="{{ route('permission_list',['id' => $user->id]) }}" class="go_bac_page" style="float: left;!important;"><span class="btn btn-blue ">بازگشت</span></a>

                        <h5>ایجاد دسترسی جدید</h5>
                        <hr>
                        @include('admin_panel.pages.error.errors_temp')

                        <form method="POST" action="{{ route('permission_store_role',['id' => $user->id]) }}" >
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">مجوز ها</label>
                                <div class="col-sm-10">
                                    <select name="perm" id="" class="form-control " data-live-search="true">
                                        <option value="0" >لطفا مجوزی را انتخاب کنید</option>
                                       @foreach($permissions as $permission)
                                        <option value="{{ $permission->id }}" onclick="select_subPer(this)">{{ $permission->name }}</option>
                                       @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">زیر مجوز ها</label>
                                <div class="col-sm-10">



                                    <select id='pre-selected-options' name="sub_per[]" multiple='multiple'>
                                        <option value='لطفا مجوزی را انتخاب کنید' selected>لطفا مجوزی را انتخاب کنید</option>
                                        <option value='لطفا مجوزی را انتخاب کنید'>لطفا مجوزی را انتخاب کنید</option>
                                    </select>



                                  {{--  <select name="زیرمجوز[]" id="framework" class="form-control selectpicker"
                                            data-live-search="true" multiple>--}}



                                       {{-- @foreach($subpermissions as $subpermission)
                                            <option value="{{ $subpermission->id }}">{{ $subpermission->name }}</option>
                                        @endforeach--}}
                                   {{-- </select>--}}

                                </div>
                            </div>


                            <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ثبت</button>



                        </form>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>
</div>



    <input id="user_id" type="text" value="{{ $user->id }}" style="display: none">

@endsection
@section('script')
    <script>
        function loadpage() {
            $('#pre-selected-options').multiSelect('refresh');
        }
        function select_subPer(s) {
            $('#pre-selected-options').children('option').remove();
            var user_id = $('#user_id').val();
            var id=   s.getAttribute('value');
            var form_data = {act : 'selectSubPermission',permission : id,user_id : user_id};
            $.ajax({
                url: '{{ route('user_select_sub_subpermission') }}', // point to server-side PHP script
                dataType: 'text', // what to expect back from the PHP script
                cache: false,
                data: form_data,
                type: 'GET',
                success: function (response) {
                var  obj = JSON.parse(response);
                var subs = obj['data']['sub_unselected'];
                var sub_selected = obj['data']['sub_selected'];
                    for (var j = 0 ; j < sub_selected.length ; j++){
                        $('#pre-selected-options').append("<option value="+ sub_selected[j]['id']  +" selected>"+sub_selected[j]['name']+"</option>");
                    }
                    for (var i = 0 ; i < subs.length ; i++){
                        $('#pre-selected-options').append("<option value="+ subs[i]['id']  +">"+subs[i]['name']+"</option>");
                    }
                         $('#pre-selected-options').multiSelect('refresh');
                },
                error: function (response) {
                }
            });
        }
    </script>
</body>
@endsection


