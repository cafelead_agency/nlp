<?php $var = 1; ?>
@extends('admin_panel.inc.master')


@section('extra')
<style>
    a{
        color: #707070 !important;
    }
    span.badge{
        background-color: #dc3545 !important;
    }
    span.caret{
        display: none;
    }
    li{
        text-align: right;
    }
    span.pull-left{
        text-align: center !important;
    }
</style>
@endsection

@section('preheader')
    <body id="body">
    @endsection


<!— Navigation Bar-->


<!— End Navigation Bar-->
@section('header')
    @include('admin_panel.inc.header')
@endsection

<!— start wrapper Bar-->
@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div id="form-holder">
                <div class="card m-b-20">
                    <div class="card-body">
                        <a href="{{ route('permission_list' ,['id' => $user->id]) }}" class="btn btn-blue fas fa-arrow-alt-circle-left go_bac_page"></a>

                        <h5>ایجاد مجوز جدید</h5>
                        <hr>

                        <form method="POSt" action="{{ route('user_store_permission',['id' => $user->id]) }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">نام</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="name" value=" " id="first_name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">شرح</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="" id="desc" name="description">
                                </div>
                            </div>



                            <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ثبت</button>



                        </form>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!— end wrapper —>
@endsection



@section('script')
</body>
@endsection

