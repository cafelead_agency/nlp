
@extends('admin_panel.inc.master')


@section('preheader')
    <body id="body">
    <i class="clock-preloader"></i>
    @endsection

    @section('header')
        @include('admin_panel.inc.header')
    @endsection



    @section('content')
        <!— start wrapper Bar-->
        <div class="wrapper">


            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <div class="add-user noPrint">

                                    <h4 class="mt-0 header-title">لیست دسترسی های {{ $user->lastname }}</h4>

                                    <div class="search-add">
                                        <form role="search" class="app-search">
                                            <div class="form-group mb-0" style="position: relative">

                                                <input type="search" data-table="order-table"
                                                       class="form-control light-table-filter" placeholder="جستجو...">


                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>

                                       {{-- <a href="{{ route('create.permission' ,['id' => $user->id]) }}"><span class="btn btn-primary ">تعریف مجوز</span></a>
                                        <a href="{{ route('create.subpermission' ,['id' => $user->id]) }}"><span class="btn btn-primary ">تعریف زیر مجوز</span></a>--}}
                                        <a href="{{ route('permission_create_role' ,['id' => $user->id]) }}"><span class="btn btn-warning ">افزودن دسترسی</span></a>
                                        <a href="{{ route('admin_users_index') }}"><span class="btn btn-blue ">بازگشت</span></a>

                                    </div>
                                </div>


                                <div id="tbl_content" class="table-rep-plugin">
                                    <img id="clock">

                                    <div class="table-responsive b-0" data-pattern="priority-columns">
                                        <table id="role-per" class="display nowrap table order-table table-striped">
                                            <thead>
                                            <tr>
                                                {{--<th class="noExport noPrint"><input type="checkbox"></th>--}}
                                                <th class="noExport"> #</th>
                                                <th data-priority="1" class="limit_width">مجوز ها</th>
                                                <th data-priority="2" class="limit_width">زیر مجوزها</th>
                                                <th data-priority="3" class="">عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbody">

                                            <?php $row_count=0; ?>
                                            @foreach($permission as $per)

                                                @php $row_count++ @endphp
                                                <tr class="tbl-row" data-row_id="{{ $row_count }}">
                                                    {{--<td class="noExport noPrint"><input type="checkbox"></td>--}}
                                                    <td class="noExport">{{ $row_count }}</td>
                                                    <td>
                                                        <ul>
                                                         {{ $per->name }}
                                                        </ul>
                                                    </td>
                                                    <td class="">
                                                        <ul>
                                                            @foreach($subs as $sub)
                                                                @foreach($sub as $s)
                                                                @if($s->permission_id == $per->id)
                                                                {{ $s->name}} <?php echo ' || '; ?>
                                                                @endif
                                                                    @endforeach
                                                            @endforeach
                                                        </ul>
                                                    </td>
                                                    <td class="noExport noPrint">
                                                        {{--<button class="delete btn btn-danger btn-group-sm fa fa-trash"--}}
                                                        {{--data-row_id="{{ $user->id }}" onclick="delete_row(this)"></button>--}}

                                                        <form action="{{ route('user_delete_permission'  , ['user_id' => $user->id,'permission' => $per->id]) }}" method="post">
                                                            {{ method_field('delete') }}
                                                            {{ csrf_field() }}
                                                            <div class="btn-group btn-group-xs">
                                                                <button class="delete btn btn-danger btn-group-sm fa fa-trash" title="حذف دسترسی"></button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>


                                        </table>

                                    </div>
                                    <div id="editor"></div>
                                </div>


                                <!--pagination-->


                                <!--pagination end-->


                            </div>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>

    @endsection

    @section('script')

    </body>


@endsection
