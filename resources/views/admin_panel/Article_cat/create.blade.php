
<?php $var = 2; ?>

@extends('admin_panel.inc.master')

@section('extra')
    <style>
        a{
            color: #707070 !important;
        }
        span.badge{
            background-color: #dc3545 !important;
        }
        span.caret{
            display: none;
        }
        li{
            text-align: right;
        }
        span.pull-left{
            text-align: center !important;
        }
    </style>
    @endsection
@section('preheader')
    <body id="body">
    @endsection

@section('header')
    @include('admin_panel.inc.header')
@endsection

@section('content')
    <!— start wrapper Bar-->

    <div class="wrapper">
        <div class="container">


            <div class="row">
                <div id="form-holder">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <a href="{{ route('Article_index_cat') }}" class="go_bac_page"><span class="btn btn-blue" style="float: left!important;">بازگشت</span></a>

                            <h5>افزودن</h5>
                            <hr>
                            @include('admin_panel.pages.error.errors_temp')

                            <form action="{{ route('Article_store_cat') }}" method="post">
                              {{csrf_field()}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">نام</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="name" value="{{ old("name") }}">
                                    </div>
                                </div>




                                <button id="user_add_btn" type="submit" class="btn btn-block btn-dark">ارسال</button>



                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!— end wrapper —>

@endsection

    @section('script')

    </body>
@endsection
