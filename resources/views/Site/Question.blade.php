<?php
$template='light';
$footer='true';
?>

@include('Site.inc.header')


<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">سوالات متداول</h2>
        <div class="faqitems">

                @foreach($db as $dbs)
                <div class="faqitem">
                    <div class="faqcontent">
                        <p>{{ $dbs->name }}</p>
                        <p>{!! $dbs->text !!}</p>
                    </div>
                    <div class="faqbutton"><span></span></div>
                </div>
                @endforeach


        </div>
    </div>
</div>
@include('Site.inc.footer')
