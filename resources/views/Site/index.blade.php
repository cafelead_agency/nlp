<?php
$header='true';
$footer='true';
$homepage='true';
?>

@include('Site.inc.header')

<?php
$site_setting = \App\Model\Site\Site_Setting::where('id' , 1)->first();
?>
<div class="karboardpopup"><div class="karboardpopupclose"></div><p></p></div>
<section class="karboard">
    <h3>کاربردهای ان.ال.پی</h3>
    <div class="karboardcircle">
        <div class="karboardright">
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/1.svg"></div>
                <p>{{ $site_setting->f_text_nlp1 }}</p>
                <span>{{ $site_setting->f_title_nlp1 }}</span>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/2.svg"></div>
                <p>{{ $site_setting->f_text_nlp2 }}</p>
                <span>{{ $site_setting->f_title_nlp2 }}</span>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/3.svg"></div>
                <p>{{ $site_setting->f_text_nlp3 }}</p>
                <span>{{ $site_setting->f_title_nlp3 }}</span>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/4.svg"></div>
                <p>{{ $site_setting->f_text_nlp4 }}</p>
                <span>{{ $site_setting->f_title_nlp4 }}</span>
            </div>
        </div>
        <div class="karboardcenter">
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/5.svg"></div>
                <span>{{ $site_setting->f_title_nlp5 }}</span>
                <p>{{ $site_setting->f_text_nlp5 }}</p>
            </div>
        </div>
        <div class="karboardleft">
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/6.svg"></div>
                <span>{{ $site_setting->f_title_nlp6 }}</span>
                <p>{{ $site_setting->f_text_nlp6 }}</p>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/7.svg"></div>
                <p>{{ $site_setting->f_text_nlp7 }}</p>
                <span>{{ $site_setting->f_title_nlp8 }}</span>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/8.svg"></div>
                <p>{{ $site_setting->f_text_nlp4 }}</p>
                <span>{{ $site_setting->f_title_nlp4 }}</span>
            </div>
            <div class="ketedami">
                <div class="keimage"><img src="/Site/img/9.svg"></div>
                <p>{{ $site_setting->f_text_nlp1 }}</p>
                <span>{{ $site_setting->f_title_nlp1 }}</span>
            </div>
        </div>


    </div>
</section>
<section class="doreha dorelide1">
    <h3>دوره ها</h3>
    <div class="container">
        <div class="dorehaselector noselect">
            <span>دوره ها</span>
            <span>کارگاه یک روزه</span>
            <span>تمامی کارگاه ها</span>
        </div>

        <div class="dorelide dorelide1x">
            <div class="dorelidesar noselect">
                @php
                    $db_dore_cat_count = 0;
                @endphp
                @foreach($db_dore_cat as $db_dore_cats)
                    <span @if($db_dore_cat_count == 0) class="active" @endif>{{ $db_dore_cats->name }}</span>
                    @php($db_dore_cat_count ++)
                @endforeach
                {{--<span class="active">تکنسین</span>--}}
                {{--<span>مهندسی</span>--}}
                {{--<span>مستری</span>--}}
                {{--<span>تکمیلی</span>--}}
            </div>
            <div class="dorelidetan">
                <?php $count = 1;?>
                @foreach($db_dore_cat as $db_dore_cats)
                    <a href="{{ route('course_dore_cat_single' , ['id' => $db_dore_cats->id]) }}" class="dorelidetang dorelidetang{{$count}} <?php if ($count == 1) echo 'active'?>">
                        <img src="{{ $db_dore_cats->img }}" draggable="false">
                    </a>
                <?php $count ++ ;?>
                @endforeach

            </div>
        </div>

        <div class="dorelide dorelide2x">
            <div class="dorelidetan">
                <div class="dorelidetang active noselect">
                    @foreach($db_one_day as $db_one_days)
                        <a href="{{ route('course_one_day_single' , ['id' => $db_one_days->id]) }}">{{ $db_one_days->name }}</a>


                    @endforeach

                </div>
            </div>
        </div>

        <div class="dorelide dorelide3x">
            <div class="dorelidetan">
                <div class="dorelidetang active noselect">
                    <a>تکنسین</a>
                    <a>اناگرام</a>
                    <a>مهندسی</a>
                    <a>کوچینگ</a>
                    <a>مستری</a>
                    <a>هیپنوتیزم</a>
                    <a>تکمیلی</a>
                    <a>کاگاه یک روزه</a>

                </div>
            </div>
        </div>


    </div>
</section>
<section class="dastavardha">
    <h3>دستاوردهای ان.ال.پی</h3>
    <div class="container">
        <div class="owl-carousel owl-dastavardha">

            @foreach($db_NLP_result as $db_NLP_results)
                <div class="dastavard"><img src="{{ $db_NLP_results->img }}" draggable="false" /></div>

                @endforeach

        </div>
    </div>
</section>
<section class="namayandegi">
    <h3>نمایندگی های کانون ان.ال.پی</h3>
    <div class="container">
        <div class="namayandegis">
            <div class="namayandegii">
                <div class="namayandegir"><img src="/Site/img/shiraz.svg"></div>
                <div class="namayandepir">
                    <div class="avatar"><img src="/Site/img/avatar.jpg"></div>
                    <h3>مازیار ایرانمهر</h3>
                    <p>میدان صدا و سیما به سمت بلوار جمهوری بین کوچه 20 و 21 پلاک 81/4</p>
                    <p>32226894</p>
                </div>
            </div>
            <div class="namayandegii">
                <div class="namayandegir"><img src="/Site/img/esfahan.svg"></div>
                <div class="namayandepir">
                    <div class="avatar"><img src="/Site/img/avatar.jpg"></div>
                    <h3>مازیار ایرانمهر</h3>
                    <p>میدان صدا و سیما به سمت بلوار جمهوری بین کوچه 20 و 21 پلاک 81/4</p>
                    <p>32226894</p>
                </div>
            </div>
            <div class="namayandegii">
                <div class="namayandegir"><img src="/Site/img/shiraz.svg"></div>
                <div class="namayandepir">
                    <div class="avatar"><img src="/Site/img/avatar.jpg"></div>
                    <h3>مازیار ایرانمهر</h3>
                    <p>میدان صدا و سیما به سمت بلوار جمهوری بین کوچه 20 و 21 پلاک 81/4</p>
                    <p>32226894</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mahsoolat dorelide2">
    <h3>محصولات</h3>
    <div class="container">
        <div class="dorehaselector noselect">

            @foreach($db_product_cat as $db_product_cats)
            <span>{{ $db_product_cats->name }}</span>

                @endforeach
        </div>

        @foreach($db_product_cat as $db_product_cats)
            <div class="mahsoolatitems0 mahsoolatitems{{$db_product_cats->id}}">
                <div class="owl-carousel owl-mahsoolat mahsoolatitems">
                    @php($product = \App\Model\Site\Product::where('cat_id' , $db_product_cats->id)->get())
                    @foreach($product as $products)
                        <a class="mahsoolatitem" href="{{ route('site_product_single' , ['id' => $products->id]) }}">
                            <div class="mahsoolatimg"><img src="{{$products->img}}" draggable="false"></div>
                            <div class="content">
                                <div class="price">{{ $products->price }} تومان</div>
                                <div class="addtocart"></div>
                            </div>
                        </a>
                        @endforeach

                </div>
            </div>
        @endforeach


    </div>
</section>
<section class="maqalaat">
    <h3>آخرین مقالات کانون</h3>
    <div class="container">
        <div class="row">

            @foreach($db_article as $db_articles)
                <a class="col maqale" href="{{ route('Article_single' , ['id' => $db_articles->id]) }}">
                    <div class="maqaleimage"><img src="{{ $db_articles->img }}"></div>
                    <h5>{{ $db_articles->name }}</h5>
                    <p>{!! limitWord($db_articles->text , 100) !!} ...</p>
                    <div class="maqaleinf"><div class="icon clock"></div>{{ $db_articles->created_at }}</div>
                    <div class="maqaleinf"><div class="icon view"></div>{{ $db_articles->count_view }} بازدید</div>
                </a>
                @endforeach

                <?php
                    function limitWord($string, $limit){
                        $words = explode(" ",$string);
                        $output = implode(" ",array_splice($words,0,$limit));
                        return $output;
                    }
                 ?>



        </div>
    </div>
</section>
<section class="nazaraat">
    <h3>نظرات شرکت کنندگان</h3>
    <div class="container">
        <div class="row nazarow owl-carousel owl-nazaraat">


           @foreach($db_Customer_Advise as $db_Customer_Advises)
                <div class="col nazarcol">
                    <div class="nazarvideo"><img src="{{ $db_Customer_Advises->img }}"></div>
                    <h4>{{ $db_Customer_Advises->name }}</h4>
                    <p>{{ $db_Customer_Advises->course_name }}</p>
                </div>
               @endforeach

        </div>
    </div>
</section>
@include('Site.inc.footer')

