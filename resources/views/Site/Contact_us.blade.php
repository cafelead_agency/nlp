<?php
$template='light footerwhite';
$footer='true';
?>


@include('Site.inc.header')
<?php
$site_setting = \App\Model\Site\Site_Setting::where('id' , 1)->first();
?>
<div class="main contactbase">
    <div class="container py-2">
        <h2 class="subtitle">تماس با ما</h2>
        <div class="contactrow">
            <div class="contactright">
                <div class="contactform">
                    <form action="{{ route('Contact_us_form') }}" method="post">
                        {{csrf_field()}}
                        <div class="inputbox">
                            <label>نام و نام خانوادگی</label>
                            <input type="text" name="name">
                        </div>
                        <div class="inputbox">
                            <label>شماره تماس</label>
                            <input type="text"name="tel">
                        </div>
                        <div class="inputbox">
                            <label>ایمیل</label>
                            <input type="text"name="email">
                        </div>
                        <textarea placeholder="پیام خود را وارد کنید." name="text"></textarea>
                        <button type="submit">ارسال</button>
                    </form>

                </div>
            </div>
            <div class="contactleft">
                <div class="maptitle">کانون ان.ال.پی</div>
                <div class="mapaddress">{{ $site_setting->address }}</div>
                <div class="mappin"></div>
                <div class="socialmedias">
                    <a href="{{ $site_setting->instagram }}" class="icon-instagram"></a>
                    <a href="{{ $site_setting->telegram }}" class="icon-telegram"></a>
                    <a href="{{ $site_setting->whatsapp }}" class="icon-whatsapp"></a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Site.inc.footer')
