<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')

<div class="main">
    <div class="tinyheader">
        <div class="container">
            <div class="tinyheaderrow">
                <div class="tinyheaderright">
                    <h2>اخبار</h2>
                </div>
                <div class="tinyheaderleft">
                    <div class="searchbox"><input type="text" placeholder="جستجو"><button></button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2 class="subtitle">مهمترین رویدادها</h2>
        <div class="tinynewses">

            @foreach($db_important as $db_importants)

                <a href="{{ route('News_single' , ['id' => $db_importants->id]) }}" class="tinynews">
                    <div class="newsimg"><img src="{{ $db_importants->img }}"></div>
                    <div class="newscontent">
                        <h4>{{ $db_importants->name }}</h4>
                        <span>{{  jdate($db_importants->created_at)->format('%d %B، %Y') }}</span>
                    </div>
                </a>
                @endforeach



        </div>
        <div class="largenewses">

            <a href="{{ route('News_single' , ['id' => $db_single[0]->id]) }}" class="largenewsesbig">
                <div class="newsimg"><img src="{{ $db_single[0]->img }}"></div>
                <div class="newscontent">
                    <h4>{{ $db_single[0]->name }}</h4>
                    <div class="newsinformations"><div class="newsinformation calendar">{{  jdate($db_single[0]->created_at)->format('%d %B، %Y') }}</div>
                        <div class="newsinformation comment">{{ $db_single[0]->count_comment }} نظر</div>
                        <div class="newsinformation eye">{{ $db_single[0]->count_view }} بازدید</div></div>
                </div>
            </a>
            <div class="largenewsesnew">
                <h2 class="subtitle">تازه ترین رویدادها</h2>


                @foreach($db_new as $db_news)

                    <a href="{{ route('News_single' , ['id' => $db_news->id]) }}" class="tinynews">
                        <div class="newsimg"><img src="{{ $db_news->img }}"></div>
                        <div class="newscontent">
                            <h4>{{ $db_news->name }}</h4>
                            <span>{{  jdate($db_news->created_at)->format('%d %B، %Y') }}</span>
                        </div>
                    </a>
                    @endforeach


            </div>

        </div>
        <div class="largenewsitems">


            @foreach($db as $dbs)

                <a href="{{ route('News_single' , ['id' => $dbs->id]) }}" class="tinynews">
                    <div class="newsimg"><img src="{{ $dbs->img }}"></div>
                    <div class="newscontent">
                        <h4>{{ $dbs->name }}</h4>
                        <div class="newsinformations"><div class="newsinformation calendar">{{  jdate($dbs->created_at)->format('%d %B، %Y') }}</div>
                            <div class="newsinformation comment">{{ $dbs->count_comment }} نظر</div>
                            <div class="newsinformation eye">{{ $dbs->count_view }} بازدید</div></div>
                    </div>
                </a>

                @endforeach

        </div>


    </div>
</div>
@include('Site.inc.footer')
