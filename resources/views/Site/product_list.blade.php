<?php
$template='light';
$footer='true';
?>
@include('Site.inc.header')

<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">محصولات کانون ان‌ال‌پی</h2>
        <div class="dorehaselector tinyselector noselect mt-3">
            @foreach($cat as $cats)
                <span>{{ $cats->name }}</span>
            @endforeach
        </div>

        <div class="products">
            @foreach($tag as $tags)
            <div class="productsub">{{ $tags->name }}</div>
            <div class="productbase owl-carousel">

                @foreach(\App\Model\Site\Product::where('tag_id' , $tags->id)->where('is_delete',0)->where('is_active' , 1)->get() as $product)
                        <a href="{{ route('site_product_single' , ['id' => $product->id]) }}" class="product">
                            <div class="productimg"><img src="{{ $product->img }}" draggable="false"></div>
                            <div class="producttitle">{{ $product->name }}</div>
                            @php($real_price = ($product->price * $product->discount) / 100)
                            @php($real_price = $product->price - $real_price)
                            <div class="productprice"><span>{{ $real_price }} تومان</span><span>{{$product->price}} تومان<b>{{$product->discount}}%</b></span></div>
                        </a>
                    @endforeach

            </div>

            @endforeach




        </div>
    </div>
</div>
@include('Site.inc.footer')
