<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')

<div class="main">
    <div class="tinyheader">
        <div class="container">
            <div class="tinyheaderrow">
                <div class="tinyheaderright">
                    <h2>اخبار</h2>
                </div>
                <div class="tinyheaderleft">
                    <div class="searchbox"><input type="text" placeholder="جستجو"><button></button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="largenewses singlepage">
            <div class="largenewsesbig">
                <div class="newscontent">
                    <h4>{{ $db->name }}</h4>
                    <div class="newsinformations"><div class="newsinformation calendar">{{  jdate($db->created_at)->format('%d %B، %Y') }}</div>
                        <div class="newsinformation comment">{{ $db->count_comment }} نظر</div>
                        <div class="newsinformation eye">{{ $db->count_view }} بازدید</div></div>
                </div>
                <div class="newsimg"><img src="{{ $db->img }}"></div>
                <p class="newsp">{!! $db->text !!}</p>

            </div>


            <div class="largenewsesnew">
                <h2 class="subtitle">تازه ترین رویدادها</h2>


                @foreach($db_new as $db_news)

                    <a href="{{ route('News_single' , ['id' => $db_news->id]) }}" class="tinynews">
                        <div class="newsimg"><img src="{{ $db_news->img }}"></div>
                        <div class="newscontent">
                            <h4>{{ $db_news->name }}</h4>
                            <span>{{  jdate($db_news->created_at)->format('%d %B، %Y') }}</span>
                        </div>
                    </a>
                    @endforeach

            </div>

        </div>



    </div>
</div>



@include('Site.inc.footer')
