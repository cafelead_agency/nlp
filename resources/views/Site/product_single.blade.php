<?php
$template='light';
$footer='true';
?>
@include('Site.inc.header')

    <div class="main dorelide1">
        <div class="container py-2">
            <h2 class="subtitle">محصولات کانون ان‌ال‌پی</h2>
            <div class="products">
                <div class="productsingle">
                    <div class="productcover">
                        <div class="productimg"><img src="{{ $db->img }}" draggable="false"></div>
                        <!-- if img less than 2 productimgselector would be remove-->


                        <div class="productimgselector">
                            @foreach($gallery as $gallerys)
                            <div><img src="{{ $gallerys->img }}"></div>

                                @endforeach
                        </div>
                    </div>
                    <div class="productcontent">
                        <div class="producttitle">{{ $db->name }}</div>
                        <h5>خلاصه ای از کتاب</h5>
                        <p>{!! $db->text !!}</p>
                        @php($real_price = ($db->price * $db->discount) / 100)
                        @php($real_price = $db->price - $real_price)
                        <div class="productprice"><span>{{ $real_price }} تومان</span><span>{{$db->price}} تومان<b>{{$db->discount}}%</b></span></div>
                        <a class="buyproduct" data-title="{{ $db->name }}" data-price="{{ $real_price }}" data-id="{{ $db->id }}">خرید</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('Site.inc.footer')
