<?php if(isset($footer) && $footer=='true'){?>

<?php
$site_setting = \App\Model\Site\Site_Setting::where('id' , 1)->first();
$phones = explode("-",$site_setting->phone);
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col">
                <h5>آخرین اطلاعیه ها</h5>
                <a href="{{ route('site_blog_single' , ['slug' => 1]) }}">شروع دوره تکنسین NLP آخر هفته ها</a>
                <a href="{{ route('site_blog_single' , ['slug' => 1]) }}">شروع دوره ی مهندسی ان ال پی صبح ها</a>
                <a href="{{ route('site_blog_single' , ['slug' => 1]) }}">شروع دوره ی تکنسین ویژه ی صبح شنبه و چهارشنبه</a>
                <a href="{{ route('site_blog_single' , ['slug' => 1]) }}">جهت ارتباط همیشگی با کانون ان ال پی</a>
                <a href="{{ route('site_blog_single' , ['slug' => 1]) }}">شروع دوره ی کوچینگ خانواده به همراه فیلم اهدای گواهینامه کوچینگ توسط رابرت دیلتز به دکتر کوروش معدلی</a>
            </div>
            <div class="col">
                <h5>دسترسی سریع به کانون</h5>
                <a href="{{ route('site_index') }}">کانون ان.ال.پی</a>
                <a href="{{ route('site_index') }}">تازه های سایت</a>
                <a href="{{ route('site_index') }}">مقالات</a>
                <a href="{{ route('course_list') }}">دوره ها</a>
                <a href="{{ route('site_index') }}">کارگاه های آموزشی</a>
                <a href="{{ route('Product_list') }}">محصولات</a>
                <a href="{{ route('Video_list') }}">ویدئو</a>
                <a href="{{ route('About_us') }}">درباره ما</a>
                <a href="{{ route('Contact_us') }}">تماس با ما</a>
            </div>
            <div class="col">
                <h5>تماس با کانون</h5>
                <p>{{ $site_setting->address }}</p>
                <h6>شماره های تماس</h6>

                @foreach($phones as $phoness)
                    <a href="#">{{ $phoness }}</a>

                @endforeach

            </div>
            <div class="col">
                <h5>خبرنامه کانون</h5>
                <p>برای آگاهی از آخرین خبرهای کانون در خبرنامه عضو شوید.</p>
                <div class="subscribe">
                    <input type="text" placeholder="آدرس ایمیل">
                    <input type="button" value="عضویت">
                </div>
                <div class="socialmetas">
                    <div class="socialmeta"><div class="icon instagram"></div><span>{{ $site_setting->instagram }}</span></div>
                    <div class="socialmeta"><div class="icon telegram"></div><span>{{ $site_setting->telegram }}</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">تمامی حقوق مالکیت معنوی این وب‌سایت برای کانون ان.ال.پی محفوظ است.</div>
</footer>
<div class="modal">
    <div class="form">
        <div class="modal_close">بستن</div>

        <input class="M_firstname" type="text" placeholder="نام">
        <input class="M_lastname" type="text" placeholder="نام خانوادگی">
        <input class="M_email" type="text" placeholder="ایمیل">
        <input class="M_phone" type="text" value="" disabled>
        <textarea placeholder="آدرس" class="M_address"></textarea>
        <textarea placeholder="توضیحات" class="M_comment"></textarea>
        <div class="overflowtitle">
            <span>تعداد</span><span>عنوان</span><span>قیمت</span>
        </div>
        <div class="overflow">
            <div class="overflowitem"><div></div><span>جمع کل</span><span><b>0</b> تومان</span></div>
        </div>
        <button class="modalsubmit">تایید اطلاعات و پرداخت</button>
    </div>
</div>
<?php } ?>
<script type="text/javascript" src="/Site/js/jquery.min.js"></script>
<script type="text/javascript" src="/Site/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/Site/js/plugin.js"></script>
</body>
</html>