<!doctype html>
<html>
<head>
    @include('Site.inc.head')
</head>

<?php
    $site_setting = \App\Model\Site\Site_Setting::where('id' , 1)->first();
?>

<body<?php if(isset($template)){echo ' class="'.$template.'"';}?>>
<div class="callmebitch freeadvice">
    <div class="icon-call"></div>
    <div class="callbox"><input type="text" placeholder="09112223344"><input type="button" value="ارسال"></div>
</div>
<div class="menullar">
</div>
<div class="menuarea">
    <div class="menu-container">
        <a href="{{ route('site_index') }}">صفحه نخست</a>
        <a href="{{ route('course_list') }}">دوره ها</a>
        <a href="{{ route('Product_list') }}">محصولات</a>
        <a href="{{ route('Article_list') }}">مقالات</a>
        <a href="{{ route('News_list') }}">اخبار</a>
        <a href="{{ route('Video_list') }}">ویدیوها</a>
        <a href="{{ route('Question_list') }}">پرسش و پاسخ</a>
        <a href="{{ route('About_us') }}">درباره ما</a>
        <a href="{{ route('Contact_us') }}">تماس با ما</a>
    </div>
</div>
<header<?php if(isset($header) && $header=='true'){ echo ' class="headeractive"';}?>>
    <div class="cartclose"></div>
    <nav>
        <div class="container">
            <div class="rightside">
                <div class="menubutton"><span></span><span></span><span></span></div>
                <a href="{{ route('site_index') }}" class="logo"></a>
            </div>
            <div class="leftside">
                <div class="cart">
                    <div class="icon-cart"><span>0</span><span>سبد خرید</span></div>
                    <div class="cartlist">
                        <div class="overflow">
                            <!--div class="overflowitem"><span>کتاب تفاسیر حاجعلی داگ</span><span>25000 تومان</span></div-->
                            <div class="overflowitem"><div></div><span>جمع کل</span><span></span></div>
                        </div>
                        <a class="cartend">تکمیل خرید</a>
                        <div class="dologin">
                            <div class="sendsms">
                                <input type="text" placeholder="شماره موبایلتان را وارد کنید">
                                <button>ارسال کد تایید</button>
                            </div>
                            <div class="givepass">
                                <div><input type="text" placeholder="کد تایید را وارد کنید"><a title="ارسال دوباره پیامک"></a></div>
                                <div><button>بررسی کد تایید</button><button>تغییر شماره</button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="primary_number">{{ $site_setting->primary_number }}</span>
                <div class="icon icon-phone"></div>
            </div>
        </div>
    </nav>
    <?php if(isset($header) && $header=='true'){?>
    <div class="container">
        <div class="headup">
            <div class="rightma">
                <h1>رسالت، اصالت و استاندارد
                    اعتبار ماست.</h1>
                <div class="headermiddle">
                    <h2>دکتر کوروش معدلی</h2>
                    <h3>بنیانگذار کانون ان.ال.پی ایران</h3>
                </div>
                <div class="headerzire">
                    <span>در رویدادهای موثر ما شرکت کنید!</span>
                    <a class="btn btnA freeadvice">مشاوره</a>
                    <a class="btn btnB" href="{{ route('course_list') }}">دوره ها</a>
                </div>
            </div>
            <div class="leftma">
                <div class="vidplayer"><img src="{{ $site_setting->primary_poster }}"></div>
            </div>
        </div>
    </div>
    <?php } ?>
</header>