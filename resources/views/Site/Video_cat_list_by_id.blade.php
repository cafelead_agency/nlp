<?php
$template='light';
$footer='true';
?>

@include('Site.inc.header')

<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">ویدئو ها</h2>

        <div class="videosrow">
            <div class="videoscat">
                <h5>دسته بندی ها</h5>
                <ul>
                    @foreach($cat as $cats)

                        <li>
                            <a href="{{ route('Video_cat_list_by_id' , ['id' => $cats->id]) }}">{{ $cats->name }}</a>

                            @php($cat_sub = \App\Model\Site\Video_cat_sub::where('cat_id' , $cats->id)->where('is_delete' , 0)->get())

                            @if($cat_sub)
                                <ul>

                                    @foreach($cat_sub as $cat_subs)
                                        <li><a href="{{ route('Video_list_by_cat_sub_id' , ['id' => $cat_subs->id]) }}">{{ $cat_subs->name }}</a></li>
                                    @endforeach

                                </ul>
                            @endif
                        </li>
                    @endforeach

                </ul>
            </div>
            <div class="videoitems">

                @foreach($db as $dbs)

                    <a href="{{ route('Video_single' , ['id' => $dbs->id]) }}" class="videoitem">
                        <div class="videopreview"><img src="{{ $dbs->img }}"></div>
                        <div class="videotitle">{{ $dbs->name }}</div>
                    </a>
                @endforeach




            </div>
        </div>
    </div>
</div>
@include('Site.inc.footer')
