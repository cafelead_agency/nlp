<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')

<div class="main dorelide1">
    <div class="tinyheader">
        <div class="container">
            <div class="tinyheaderrow">
                <div class="tinyheaderright">
                    <div class="datetime">{{ $db->created_at }}</div>
                    <h2>مقالات کانون ان.ال.پی</h2>
                </div>
                <div class="tinyheaderleft">
                    <div class="searchbox"><input type="text" placeholder="جستجو"><button></button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-2">

        <div class="blogsingle">
            <div class="blogsingle-header"><img src="{{ $db->img }}"><h4>{{ $db->name }}</h4></div>
            <div class="blogsingle-titles">
                <a>فهرست عناوین</a>
                @php
                    $list_array = explode('-' , $db->list);

                 @endphp
                @foreach($list_array as $list_arrays)

                    <a>{{ $list_arrays }}</a>
                @endforeach


            </div>
            <div class="blogsingle-content">
                <div class="blogsinglehead">
                    <h3>{{ $db->name }}</h3>
                    <div class="haveicon calendar">{{ $db->created_at }}</div>
                    <div class="haveicon eye">{{ $db->count_view }} بازدید</div>
                </div>
                <p class="paragraph">
                    {!! $db->text !!}
                </p>

                <div class="addcomment"><span>درباره این مقاله نظر بدهید.</span><a>ارسال نظر</a></div>
                <div class="comments">
                    <h5>نظر در مورد این مقاله</h5>
                    <div class="comment">
                        <div class="commentheader"><div class="avatar"><img src="/Site/img/avatar.jpg"></div><span>خسرو محمودی</span></div>
                        <p>
                            به جرات میشه گفت یک مرجع کامل و مفید هستید و هروز ازتون نکته های مهمی رو یاد می گیرم<br/>
                            کسایی رو که مثل من در فکر و برنامه ریزی استارت زدن یک سایت موفق هستن شما بهترین راهنما برایشان هستید<br/>
                            کسایی رو که مثل من در فکر و برنامه ریزی استارت زدن یک سایت موفق هستن شما بهترین راهنما برایشان هستید<br/><br/>
                            خسته نباشید و ممنونم</p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
@include('Site.inc.footer')
