<?php
$template='light';
$footer='true';

?>

@include('Site.inc.header')

<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">{{ $db->name }}</h2>
        <div class="kujam">
            <a href="{{ route('course_list') }}">دوره ها</a>
            <a href="#">{{ $db->name }}</a>
        </div>
        <div class="duresingle">
            <div class="duresingleheader"><img src="{{ $db->img }}" draggable="false"></div>
            <div class="durep">
                <div class="dureinfo">
                    <div class="dureinfocol">جزئیات دوره</div>
                    <div class="dureinfocol">مهلت ثبت نام</div>
                    <div class="dureinfocol">تعداد</div>

                    @foreach(\App\Model\Site\Course_dore_cat_sub::where('course_dore_cat_id', $db->id)->where('is_delete', 0)
                    ->where('is_active', 1)->get() as  $subs)
                            <div class="dureinfocol">{{ $subs->name }}</div>
                            <div class="dureinfocol">{{ $subs->expire }}</div>
                            <div class="dureinfocol"><select>
                                    <option value="0">تعداد</option>
                                    <option value="1">یک نفر</option>
                                    <option value="2">دو نفر</option>
                                </select></div>
                        @endforeach




                    <div class="dureinfocol"><input type="text" placeholder="کد معرف"><button>اعمال</button></div>
                    <div class="dureinfocol"><input type="button" value="ثبت نام"></div>
                </div>
                <div class="durecontent">
                    <div class="duretabs">
                        <a class="active">{{ $db->name }} چیست؟</a>
                        <a>مطالب دوره</a>
                        <a>مناسب چه کسانی است؟</a>
                        <a>درباره مدرس</a>
                        <a>گالری تصاویر</a>
                        <a>سوالات</a>
                    </div>
                    <div class="duretab duretab-1 active">
                        {!! $db->info !!}
                    </div>
                    <div class="duretab duretab-2">
                        {!! $db->about !!}
                    </div>
                    <div class="duretab duretab-3">
                        {!! $db->sweet !!}
                    </div>
                    <div class="duretab duretab-4">
                        {!! $db->teacher !!}
                    </div>
                    <div class="duretab duretab-5">
                        <div class="doreyerooze owl-carousel takowl mb-0">
                            @foreach(\App\Model\Site\Course_dore_cat_gallery::where('course_dore_cat_id', $db->id)->where('is_delete', 0)
                                     ->where('is_active', 1)->get() as  $gallery)
                                <div class="doreyeroozeitem"><img src="{{ $gallery->img }}"></div>

                                @endforeach


                        </div>
                    </div>
                    <div class="duretab duretab-6">


                        @foreach(\App\Model\Site\Course_dore_cat_question::where('course_dore_cat_id', $db->id)->where('is_delete', 0)
                                     ->where('is_active', 1)->get() as  $question)
                            <div class="faqitem">
                                <div class="faqcontent">
                                    <p>{{ $question->title }}</p>
                                    <p>{{$question->text}} </p>
                                </div>
                                <div class="faqbutton"><span></span></div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="duretakhfif">
                    <h4>تخفیفات ویژه</h4>
                    <div class="duretable">
                        <div class="duretablecol">دوره</div>
                        <div class="duretablecol">مبلغ تخفیف</div>
                        <div class="duretablecol">تکنسین</div>
                        <div class="duretablecol">100000 تومان</div>
                        <div class="duretablecol">هیپنوتیزم اریکسونی</div>
                        <div class="duretablecol">300000 تومان</div>
                        <div class="duretablecol">مهندسی ان ال پی</div>
                        <div class="duretablecol">500000 تومان</div>
                        <div class="duretablecol">مستری ان ال پی</div>
                        <div class="duretablecol">500000 تومان</div>
                        <div class="duretablecol">اناگرام تخصصی</div>
                        <div class="duretablecol">500000 تومان</div>
                    </div>
                    <p><span>10%</span>با پیش ثبت نام در دوره های جاری از تخفیف 10% در کل شهریه برخوردار میباشد.</p>
                    <p><span>15%</span>شرکت در 2 کارگاه آموزشی کانون میتوانید از 15% تخفیف در کارگاه سوم بهره مند شوید.</p>
                    <a href="#" class="mushavere">مشاوره</a>
                </div>

            </div>
        </div>

    </div>
</div>
@include('Site.inc.footer')
