<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')


<div class="main dorelide1">
    <div class="tinyheader">
        <div class="container">
            <div class="tinyheaderrow">
                <div class="tinyheaderright">
                    <div class="datetime">اردیبهشت دیروز بعدازظهر</div>
                    <h2>مقالات کانون ان.ال.پی</h2>
                </div>
                <div class="tinyheaderleft">
                    <div class="searchbox"><input type="text" placeholder="جستجو"><button></button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-2">
        <div class="blogcat">
            @foreach($cat as $cats)
                <a href="{{ route('Article_list_by_cat' , ['cat_id' => $cats->id]) }}">{{ $cats->name }}</a>
            @endforeach
        </div>

        <div class="postbox catbox">

            @foreach($db as $dbs)
                <a href="{{ route('Article_single' , ['id' => $dbs->id]) }}" class="post">
                    <img src="{{ $dbs->img }}">
                    <div class="information">
                        <h4>{{ $dbs->name }}</h4>
                        <div class="haveicon calendar">{{ $dbs->created_at }}</div>
                        <div class="haveicon eye">{{ $dbs->count_view }} بازدید</div>
                    </div>
                    <div class="titles">

                        @php
                            $list_array = explode('-' , $dbs->list);

                        @endphp
                        @foreach($list_array as $list_arrays)

                            <span>{{ $list_arrays }}</span>
                        @endforeach


                    </div>
                </a>

                @endforeach



        </div>


    </div>
</div>
@include('Site.inc.footer')
