<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')


<div class="main dorelide1">
    <div class="tinyheader">
        <div class="container">
            <div class="tinyheaderrow">
                <div class="tinyheaderright">
                    <div class="datetime">اردیبهشت دیروز بعدازظهر</div>
                    <h2>مقالات کانون ان.ال.پی</h2>
                </div>
                <div class="tinyheaderleft">
                    <div class="searchbox"><input type="text" placeholder="جستجو"><button></button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-2">
        <div class="blogcat">
            @foreach($cat as $cats)
                    <a href="{{ route('Article_list_by_cat' , ['cat_id' => $cats->id]) }}">{{ $cats->name }}</a>
                @endforeach
            {{--<a>کوچینگ</a>--}}
            {{--<a>ان.ال.پی</a>--}}
            {{--<a>هیپنوتیزم</a>--}}
            {{--<a>موفقیت</a>--}}
            {{--<a>آرشیو</a>--}}
        </div>
        @php
            $count_postbox = 0;


        @endphp
        @foreach($db as $dbs)


        @if($count_postbox == 0)
        <div class="postbox">
            <a href="{{ route('Article_single' , ['id' => $dbs->id]) }}" class="post">
                <img src="{{ $dbs->img }}">
                <div class="information">
                    <h4>{{ $dbs->name }}</h4>
                    <div class="haveicon calendar">{{ $dbs->created_at }}</div>
                    <div class="haveicon eye">{{ $dbs->count_view }} بازدید</div>
                </div>
            </a>
            @endif
                @if($count_postbox == 1)
                     <div class="postnox">
                @endif
                    @if($count_postbox == 1 | $count_postbox == 2)
                    <a href="{{ route('Article_single' , ['id' => $dbs->id]) }}" class="post">
                        <img src="{{ $dbs->img }}">
                        <div class="information">
                            <h4>{{ $dbs->name }}</h4>
                            <div class="haveicon calendar">{{ $dbs->created_at }}</div>
                            <div class="haveicon eye">{{ $dbs->count_view }} بازدید</div>
                        </div>
                    </a>
                             @php($count_postbox ++)
                     @endif
                @if($count_postbox == 3)
                    </div>
                @php($count_postbox = 4)

            @endif
            @if($count_postbox == 4)
        </div>
            @endif
            @if($count_postbox == 0)
                    @php($count_postbox = 1)
                @endif

            @if($count_postbox == 4)
                @php($count_postbox = 0)
            @endif

        @endforeach


        {{--<div class="postbox">--}}
            {{--<a href="blog-single.php" class="post">--}}
                {{--<img src="img/blog1.jpg">--}}
                {{--<div class="information">--}}
                    {{--<h4>کوچینگ و ناخودآگاه</h4>--}}
                    {{--<div class="haveicon calendar">دیروز، یک اردیبهشت</div>--}}
                    {{--<div class="haveicon eye">۱۶۳ بازدید</div>--}}
                {{--</div>--}}
            {{--</a>--}}
            {{--<div class="postnox">--}}
                {{--<a href="blog-single.php" class="post">--}}
                    {{--<img src="img/blog2.jpg">--}}
                    {{--<div class="information">--}}
                        {{--<h4>کوچینگ و ناخودآگاه</h4>--}}
                        {{--<div class="haveicon calendar">دیروز، یک اردیبهشت</div>--}}
                        {{--<div class="haveicon eye">۱۶۳ بازدید</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
                {{--<a href="blog-single.php" class="post">--}}
                    {{--<img src="img/blog3.jpg">--}}
                    {{--<div class="information">--}}
                        {{--<h4>کوچینگ و ناخودآگاه</h4>--}}
                        {{--<div class="haveicon calendar">دیروز، یک اردیبهشت</div>--}}
                        {{--<div class="haveicon eye">۱۶۳ بازدید</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}


    </div>
</div>

@include('Site.inc.footer')
