<?php
$template='light';
$footer='true';

?>


@include('Site.inc.header')

<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">دوره های ان.ال.پی</h2>
        <div class="dorehaselector noselect mt-2">
            <span>دوره ها</span>
            <span>کارگاه یک روزه</span>
            <span>تمامی کارگاه ها</span>
        </div>
        <div class="dorelide dorelide1x">


        @foreach($db_dore_cat as $db_dore_cats)
            <div class="doreliderow">
                <div class="dorelidesar noselect">
                    <span class="active titletive">{{ $db_dore_cats->name }}</span>
                </div>
                <div class="dorelidetan">
                    <a href="{{ route('course_dore_cat_single' , ['id' => $db_dore_cats->id ]) }}" class="dorelidetang dorelidetang1 active"><img src="{{ $db_dore_cats->img }}" draggable="false"></a>
                </div>
            </div>


            @endforeach





        </div>


        <div class="dorelide dorelide2x">


            <div class="doreyeroozebase">
                @foreach($db_one_day as $db_one_days)

                    <h5 class="doreyeroozetitle">{{ $db_one_days->name }}</h5>
                    <div class="doreyerooze owl-carousel takowl">

                        @foreach(\App\Model\Site\Course_one_day_gallery::where('course_dore_cat_id', $db_one_days->id)->where('is_delete', 0)
                                     ->where('is_active', 1)->get() as  $gallery)
                                <a href="{{ route('course_one_day_single' , ['id' => $db_one_days->id]) }}" class="doreyeroozeitem">
                                    <img src="{{ $gallery->img }}">
                                </a>
                            @endforeach

                    </div>
                    @endforeach
                {{--<h5 class="doreyeroozetitle">کارگاه رفع استرس</h5>--}}
                {{--<div class="doreyerooze owl-carousel takowl">--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider.jpg">--}}
                    {{--</a>--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider2.jpg">--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<h5 class="doreyeroozetitle">کارگاه ازدواج موفق</h5>--}}
                {{--<div class="doreyerooze owl-carousel takowl">--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider2.jpg">--}}
                    {{--</a>--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider.jpg">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<h5 class="doreyeroozetitle">خواب و خلسه</h5>--}}
                {{--<div class="doreyerooze owl-carousel takowl">--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider.jpg">--}}
                    {{--</a>--}}
                    {{--<a href="dure-single.php" class="doreyeroozeitem">--}}
                        {{--<img src="http://localhost/nlp/img/slider2.jpg">--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>


        </div>
        <div class="dorelide dorelide3x">

            <div class="durecircle">

                <div class="durecirclesquare">
                    <div class="durecirclesquarepapa">تکمیلی</div>
                    <div class="durecirclesquarelist">
                        <a href="dure-single.php">کوچینگ کسب و کار</a>
                        <a href="dure-single.php">کوچینگ خانواده</a>
                    </div>
                </div>

                <div class="durecirclesquare">
                    <div class="durecirclesquarepapa">تکنسین</div>
                    <div class="durecirclesquarelist">
                        <a href="dure-single.php">کوچینگ کسب و کار</a>
                        <a href="dure-single.php">کوچینگ خانواده</a>
                    </div>
                </div>

                <div class="durecirclesquare">
                    <div class="durecirclesquarepapa">مهندسی</div>
                    <div class="durecirclesquarelist">
                        <a href="dure-single.php">کوچینگ کسب و کار</a>
                        <a href="dure-single.php">کوچینگ خانواده</a>
                    </div>
                </div>

                <div class="durecirclesquare">
                    <div class="durecirclesquarepapa">کوچینگ</div>
                    <div class="durecirclesquarelist">
                        <a href="dure-single.php">کوچینگ کسب و کار</a>
                        <a href="dure-single.php">کوچینگ خانواده</a>
                    </div>
                </div>

                <div class="durecirclesquare">
                    <div class="durecirclesquarepapa">مستری</div>
                    <div class="durecirclesquarelist">
                        <a href="dure-single.php">کوچینگ کسب و کار</a>
                        <a href="dure-single.php">کوچینگ خانواده</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@include('Site.inc.footer')
