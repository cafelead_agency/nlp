<?php
$template='dark';
$footer='true';
?>
@include('Site.inc.header')

<div class="main dorelide1">
    <div class="container py-2">
        <h2 class="subtitle">درباره ما</h2>
        <div class="about">
            <div class="aboutgallery">

                @foreach($people as $p)
                    <div class="aboutgalleryimg"><img src="{{$p->img}}"><span>{{ $p->name }}</span></div>


                @endforeach

            </div>

           {!! $co->text !!}

        </div>
    </div>
</div>
@include('Site.inc.footer')
